#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>

static const char *embb_c_tpl =
  "#include <stdlib.h>\n"
  "#include <string.h>\n"
  "typedef struct Blob {\n"
  "  const char *path;\n"
  "  const char *blob;\n"
  "  int len;         \n"
  "} Blob;            \n"
  "int blobcmp(const void *p0, const void *p1) {\n"
  "  char *b0 = (char *) p0;\n"
  "  Blob *b1 = (Blob *) p1;\n"
  "  return strcmp(b0, b1->path);\n"
  "}\n"
  "extern Blob blobs[];\n"
  "#define nblobs %d\n"
  "const char *embb(const char *key, size_t *len) {\n"
  "  Blob *b = bsearch(key, blobs, nblobs, sizeof *blobs, blobcmp);\n"
  "  if (b) {\n"
  "    *len = b->len;\n"
  "    return b->blob;\n"
  "  }\n"
  "  return NULL;\n"
  "}\n"
  "Blob blobs[nblobs] = {";

typedef unsigned char uchar;
typedef struct {
  const char *path;
  uchar *blob;
  size_t len, capa;
} Blob;

void usage() {
  fputs("Usage: embbin FILE...\n", stderr);
  exit(0);
}

void error(int fatal, char *err, ...) {
  va_list args;
  va_start(args, err);
  vfprintf(stderr, err, args);
  va_end(args);
  if (err[strlen(err) - 1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  if (fatal)
    exit(1);
}

void *ealloc(size_t nbytes) {
  void *p = calloc(nbytes, 1);
  if (p == NULL)
    error(1, "Cannot allocate %zd bytes:", nbytes);
  return p;
}

void *erealloc(void *p, size_t nbytes) {
  void *p2 = realloc(p, nbytes);
  if (p2 == NULL)
    error(1, "Cannot re-allocate %zd bytes:", nbytes);
  return p2;
}

void *addbytes(void *dst, size_t *len, size_t *capa, void *src, size_t nb) {
  if (dst == NULL) {
    dst = ealloc(nb);
    *capa = nb;
    *len = 0;
  }
  if (*len + nb > *capa) {
    while (*len + nb > *capa)
      *capa <<= 1;
    dst = erealloc(dst, *capa);
  }
  memmove(dst + *len, src, nb);
  *len += nb;
  return dst;
}

Blob *process(const char *path) {
  int nread;
  char buf[0xffff];
  FILE *fp;
  Blob *blob = ealloc(sizeof *blob);
  blob->path = path;
  fp = fopen(path, "rb");
  if (fp == NULL) {
    error(0, "Cannot open `%s':", path);
    free(blob);
    return NULL;
  }
  while ((nread = fread(buf, 1, sizeof buf, fp)) != 0)
    blob->blob = addbytes(blob->blob, &blob->len, &blob->capa, buf, nread);
  if (ferror(fp)) {
    error(0, "Failed to read %s:", path);
    free(blob->blob);
    free(blob);
    return NULL;
  }
  return blob;
}

int blobcmp(const void *p0, const void *p1) {
  Blob *b0 = *(Blob **) p0;
  Blob *b1 = *(Blob **) p1;
  return strcmp(b0->path, b1->path);
}

char *bin2c(uchar *bin, size_t len) {
  int i;
  char buf[6], *s = ealloc(len * 5 + 3), *p = s;
  for (i = 0; i < len; ++i) {
    sprintf(buf, "\\x%x", bin[i]);
    strcat(p, buf);
    p += strlen(buf);
  }
  return s;
}

FILE *startcfile(int nblobs) {
  FILE *fp = fopen("embb.c", "w");
  if (fp == NULL)
    error(1, "Cannot open `embb.c' for writing:");
  fprintf(fp, embb_c_tpl, nblobs);
  return fp;
}

void add2cfile(FILE *fp, Blob *blob, int first) {
  char *carr = bin2c(blob->blob, blob->len);
  fprintf(fp, "%s\n{ \"%s\", \"%s\", %zd }", first ? "" : ",",
          blob->path, carr, blob->len);
  free(carr);
}

void endcfile(FILE *fp) {
  fprintf(fp, "};\n");
  fclose(fp);
}

void generate(Blob **blobs, int nblobs) {
  int i;
  FILE *fp = startcfile(nblobs);
  qsort(blobs, nblobs, sizeof *blobs, blobcmp);
  for (i = 0; i < nblobs; ++i)
    add2cfile(fp, blobs[i], i == 0);
  endcfile(fp);
}

int main(int argc, char **argv) {
  int i, nblobs = 0;
  Blob **blobs = NULL;
  if (argc < 2)
    usage();
  blobs = ealloc((argc-1) * sizeof *blobs);
  for (i = 1; i < argc; ++i) {
    Blob *blob = process(argv[i]);
    if (blob)
      blobs[nblobs++] = blob;
  }
  generate(blobs, nblobs);
  return 0;
}
