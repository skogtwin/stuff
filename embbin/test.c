#include <stdio.h>

int main() {
  size_t len, nbytes;
  const char *blob;
  extern const char *embb(const char *, size_t *);
  FILE *fp = fopen("test_blob", "wb");
  if ((blob = embb("embbin", &len)) == NULL) {
    printf("embbin not found!\n");
    return 0;
  }
  if ((nbytes = fwrite(blob, 1, len, fp)) == 0)
    perror("fwrite");
  printf("%zd/%zd bytes were written\n", nbytes, len);
  fclose(fp);
  return 0;
}
