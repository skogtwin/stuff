#!/bin/sh

function fail {
  echo Fail
  exit 1
}

function pass {
  echo Pass
  exit
}

if [ -z "$CC" ] ; then CC=gcc ; fi

$CC -o embbin embbin.c && ./embbin embbin && $CC -o test test.c embb.c
if [ $? -ne 0 ] ; then
  fail
fi

./test && diff embbin test_blob
if [ $? -ne 0 ] ; then
  fail
fi
pass
