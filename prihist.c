#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

const char *progname = "prihist";
void usage() {
  fprintf(stderr,
          "Usage: %s [options] [FILE...]\n"
          "Draws a histogram from a list of key-value pairs, "
          "one pair per line,\n"
          "value first (e.g. output of `uniq -c'). Keys could be ommited.\n"
          "Options:\n"
          "   -w <width>  limit output width to <width> chars\n"
          "   -s <string> split key and values on <string> instead of space\n"
          "   -q          supress output of non-critical errors\n"
          "   -h          print this message and exit\n",
          progname);
  exit(1);
}

static int output_width = 80;
static char *sep = " ";
static int quiet = 0;

/**********************************************************************
 * no real cross-paltform solution for detecting tty width;
 * here be ifdefs
 */

#if defined(_WIN32) || defined(WIN32)

#include <windows.h>
#include <io.h>

int istty() {
  return _isatty(_fileno(stdout));
}

int tty_width() {
  CONSOLE_SCREEN_BUFFER_INFO info;
  /* O_o */
  if (!GetConsoleScreenBufferInfo(GetStdHandle(STDOUT_OUTPUT_HANDLE), &info))
    return 80;
  return info.dwSize.X;
}

#elif defined(__unix__) || defined(__unix) || defined(__MACH__)

#include <sys/ioctl.h>
#include <unistd.h>

int istty() {
  return isatty(STDOUT_FILENO);
}

int tty_width() {
  struct winsize w;
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &w) == -1)
    return 80;
  return w.ws_col;
}

#else

int istty() { return 1; }
int tty_width() { return 80; }

#endif

/* no more ifdefs ******************************************************/

typedef struct Hist_ {
  int sz, capa;
  char **keys;
  double *vals;
} Hist;

void error(int fatal, char *err, ...) {
  va_list args;
  if (quiet && !fatal)
    return;
  va_start(args, err);
  if (progname)
    fprintf(stderr, "%s: ", progname);
  vfprintf(stderr, err, args);
  va_end(args);
  if (err[strlen(err) - 1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  if (fatal)
    exit(1);
}

void *ealloc(size_t nbytes) {
  void *p = calloc(nbytes, 1);
  if (p == NULL)
    error(1, "Cannot allocate %zd bytes:", nbytes);
  return p;
}

void *erealloc(void *p, size_t nbytes) {
  void *p2 = realloc(p, nbytes);
  if (p2 == NULL)
    error(1, "Cannot re-allocate %zd bytes:", nbytes);
  return p2;
}

char *estrdup(const char *s) {
  char *t = ealloc(strlen(s) + 1);
  return strcpy(t, s);
}

char *trim(char *s) {
  char *p = s + strlen(s) - 1;
  while (p > s && isspace(*p))
    *p-- = '\0';
  p = s;
  while (isspace(*p))
    ++p;
  return p;
}

void add_metrics(Hist *h, char *k, double v) {
  if (h->capa == 0) {
    h->keys = ealloc(sizeof *h->keys);
    h->vals = ealloc(sizeof *h->vals);
    h->capa = 1;
  } else if (h->sz >= h->capa) {
    h->capa *= 2;
    h->keys = erealloc(h->keys, h->capa * sizeof *h->keys);
    h->vals = erealloc(h->vals, h->capa * sizeof *h->vals);
  }
  h->keys[h->sz] = estrdup(k);
  h->vals[h->sz] = v;
  h->sz++;
}

void cleanup_hist(Hist *h) {
  int i;
  for (i = 0; i < h->sz; ++i)
    free(h->keys[i]);
  free(h->keys);
  free(h->vals);
}

char *hist_max_key(Hist *h) {
  int i, max_len = 0;
  char *max = NULL;
  for (i = 0; i < h->sz; ++i) {
    int len = strlen(h->keys[i]);
    if (len > max_len) {
      max  = h->keys[i];
      max_len = len;
    }
  }
  return max;
}

double hist_max_val(Hist *h) {
  int i;
  double max = 0.;
  for (i = 0; i < h->sz; ++i)
    if (h->vals[i] > max)
      max = h->vals[i];
  return max;
}

int hist_longest_val(Hist *h) {
  int i, len;
  int max = 0;
  for (i = 0; i < h->sz; ++i) {
    len = snprintf(NULL, 0, "%g", h->vals[i]);
    if (len > max)
      max = len;
  }
  return max;
}

void print_hist(Hist *h) {
  int i, j;
  int max_key_len = strlen(hist_max_key(h));
  double max_val = hist_max_val(h);
  int longest_val = hist_longest_val(h);
  int hist_len = output_width - max_key_len - longest_val - 2;
  double cell_value = max_val / hist_len;
  for (i = 0; i < h->sz; ++i) {
    int row_len = h->vals[i] / cell_value;
    printf("%*s #", max_key_len, h->keys[i]);
    for (j = 1; j < row_len; ++j)
      fputc('#', stdout);
    printf(" %.*g\n", longest_val, h->vals[i]);
  }
}

void hist(FILE *fp, const char *name) {
  double val;
  char buf[80], *p, *split;
  Hist h = {0};
  while (fgets(buf, sizeof buf, fp)) {
    p = trim(buf);
    if (*p == '\0')
      continue;
    if ((split = strstr(p, sep)) != NULL) {
      *split = '\0';
      add_metrics(&h, trim(split + 1), atof(p));
    } else if ((val = atof(p)) != 0.0) {
        add_metrics(&h, "", val);
    } else {
        error(0, "Malformed string `%s'", buf);
    }
  }
  if (name)
    printf("%s:\n", name);
  print_hist(&h);
  cleanup_hist(&h);
}

int main(int argc, char **argv) {
  int i, print_fnames;
  if (istty())
    output_width = tty_width();
  for (i = 1; i < argc; ++i) {
    if (argv[i][0] != '-')
      break;
    if (strcmp(argv[i], "-h") == 0) {
      usage();
    } else if (strcmp(argv[i], "-s") == 0) {
      if (++i == argc)
        usage();
      sep = argv[i];
    } else if (strcmp(argv[i], "-w") == 0) {
      if (++i == argc)
        usage();
      output_width = atoi(argv[i]);
    } else if (strcmp(argv[i], "-q") == 0) {
      quiet = 1;
    }
  }
  if (i == argc)
    hist(stdin, NULL);
  else {
    print_fnames = argc - i > 1;
    for (; i < argc; ++i) {
      FILE *fp = fopen(argv[i], "r");
      if (fp == NULL)
        error(0, "Cannot open file %s:", argv[i]);
      else {
        hist(fp, print_fnames ? argv[i] : NULL);
        fclose(fp);
      }
    }
  }
  return 0;
}
