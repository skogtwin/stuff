#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define nelems(x) (sizeof(x) / sizeof((x)[0]))
#define ROMAN -1

const char alphabet[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

static const struct {
  const char *name;
  int val;
} digits[] = {
  { "M", 1000 }, { "CM", 900 }, { "D", 500 }, { "CD", 400 }, { "C", 100 },
  { "XC", 90 }, { "X", 10 }, { "IX", 9 }, { "V", 5 }, { "IV", 4 }, { "I", 1 }
};

void usage() {
  fprintf(stderr, "Usage: prno IN_BASE OUT_BASE NUMBER...\n"
                  "Converts numbers from base IN_BASE to base OUT_BASE.\n"
                  "Supports bases from 2 to 36 and `R' for roman numerals.\n");
  exit(1);
}

void ul2str(char *dst, int len, unsigned long n, int base) {
  int i, j;
  if (len < 2) /* minimum { '0', '\0' } */
    return;
  for (i = 0; i < len && n != 0; n /= base, ++i)
    dst[i] = alphabet[n % base];
  if (i == 0)
    dst[i++] = alphabet[0];
  dst[i] = '\0';
  for (j = i-1, i = 0; i < j; ++i, --j) {
    int c = dst[i];
    dst[i] = dst[j];
    dst[j] = c;
  }
}

void ul2roman(char *buf, int len, unsigned long num) {
  unsigned long n = num;
  int i, m;
  char *p0 = buf, *p1 = buf + len - 1;
  *p0 = '\0';
  for (i = 0; i < nelems(digits); ++i) {
    while ((m = n - digits[i].val) >= 0) {
      int romanlen = strlen(digits[i].name);
      if (p0 + romanlen + 1 >= p1) {
        fprintf(stderr,"prno: number is too long for roman numerals: %lu", num);
        exit(1);
      }
      strcat(p0, digits[i].name);
      p0 += romanlen;
      n = m;
    }
  }
}

int findroman(char **s) {
  int i;
  for (i = 0; i < nelems(digits); ++i) {
    int len = strlen(digits[i].name);
    if (strncasecmp(*s, digits[i].name, len) == 0) {
      *s += strlen(digits[i].name);
      return digits[i].val;
    }
  }
  return 0;
}

long roman2l(char *s) {
  int dig, sign = 1;
  long n = 0;
  char *p = s;
  while (isspace(*p))
    ++p;
  if (*p == '-') {
    sign = -1;
    ++p;
  }
  while ((dig = findroman(&p)) != 0)
    n += dig;
  return n * sign;
}

int main(int argc, char **argv) {
  int i, sign, src_base = 10, dst_base = 16;
  char buf[0x200];

  if (argc < 4)
    usage();
  src_base = (strcasecmp(argv[1], "r") == 0) ? ROMAN : atol(argv[1]);
  dst_base = (strcasecmp(argv[2], "r") == 0) ? ROMAN : atol(argv[2]);
  if (!src_base || src_base > (int) sizeof(alphabet)) {
    fprintf(stderr, "Bad source base: %s\n", argv[1]);
    exit(1);
  }
  if (!dst_base || dst_base > (int) sizeof(alphabet)) {
    fprintf(stderr, "Bad destination base: %s\n", argv[2]);
    exit(1);
  }

  for (i = 3; i < argc; ++i) {
    long n;
    n = src_base == ROMAN ? roman2l(argv[i]) : strtol(argv[i], NULL, src_base);
    if (n < 0) {
      putchar('-');
      n = -n;
    }
    if (dst_base == ROMAN)
      ul2roman(buf, sizeof(buf), (unsigned long) n);
    else
      ul2str(buf, sizeof(buf), (unsigned long) n, dst_base);
    printf("%s\n", buf);
  }
  return 0;
}
