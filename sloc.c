/* sloc.c - output significant lines of code.
 * I.e. strip empty lines and commentaries.
 *
 * This file is distributed under MIT license
 * http://opensource.org/licenses/MIT
 */

#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static const char *progname = "sloc";
static void usage(int rc) {
  FILE *fp = rc ? stderr : stdout;
  fprintf(fp, "usage: %s [options] [file...]\n", progname);
  fprintf(fp, "output significant lines of code, i.e. skip empty lines and commentaries.\n");
  fprintf(fp, "if no files given, reads from stdin.\n");
  fprintf(fp, "options:\n"
    "   -s LANGUAGE  syntax, LANGUAGE is one of c, python, lua\n"
    "   -h           help, print this message and exit\n"
  );
  exit(rc);
}

int error(int fatal, char *err, ...) {
  va_list args;
  va_start(args, err);
  if (progname)
    fprintf(stderr, "%s: ", progname);
  vfprintf(stderr, err, args);
  va_end(args);
  if (err[strlen(err) - 1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  if (fatal)
    exit(1);
  return fatal;
}

/*****************************************************************************/

typedef enum {
  C,
  LUA,
  PYTHON
} Syntax;

typedef enum {
  REG,
  BLOCKCOMM,
  STR
} State;

void print(char *l, char *r) {
  char *p = r;
  while (p >= l && isspace(*p))
    --p;
  if (p < l)
    return;
  printf("%.*s%s", p-l+1, l, *r == '\n' && *p != '\n' ? "\n" : "");
}

char *bcopen(char *tok, size_t toksz, const char *s, Syntax syntax) {
  switch (syntax) {
  case C:
    if (strncmp(s, "/*", 2) == 0) {
      if (toksz < 3)
        error(-1, "too long comment start token");
      strcpy(tok, "/*");
      return tok;
    }
    break;
  case LUA:
    if (strncmp(s, "--", 2) == 0 && s[2] == '[') {
      int i = 3;
      while (s[i] == '=')
        ++i;
      if (s[i] == '[') {
        ++i;
        if (toksz < i+1)
          error(-1, "too long comment start token");
        strncpy(tok, s, i);
        tok[i] = '\0';
        return tok;
      }
    }
    break;
  case PYTHON: /* no block comments in Python */
    break;
  }
  return NULL;
}

int bcclos(const char *s, const char *tok, Syntax syntax) {
  switch (syntax) {
  case C:
    if (strncmp(s, "*/", 2) == 0)
      return 2;
    break;
  case LUA:
    if (s[0] == ']') {
      int i = 1;
      while (s[i] == '=' && tok[2+i] == '=')
        ++i;
      if (s[i] == ']' && tok[2+i] == '[') {
        return i+1;
      }
    }
    break;
  case PYTHON: /* no block comments in Python */
    break;
  }
  return -1;
}

char *stropen(char *tok, size_t toksz, const char *s, Syntax syntax) {
  switch (syntax) {
  case C:
    if (*s == '\'' || *s == '"') {
      if (toksz < 2)
        error(-1, "too long string start token");
      tok[0] = *s; tok[1] = '\0';
      return tok;
    }
    break;
  case LUA:
    if (*s == '"' || *s == '\'') {
      if (toksz < 2)
        error(-1, "too long string start token");
      tok[0] = *s; tok[1] = '\0';
      return tok;
    }
    if (*s == '[') {
      int i = 1;
      while (s[i] == '=')
        ++i;
      if (s[i] == '[') {
        ++i;
        if (toksz < i+1)
          error(-1, "too long string start token");
        strncpy(tok, s, i);
        tok[i] = '\0';
        return tok;
      }
    }
    break;
  case PYTHON:
  }
  return NULL;
}

int unescaped(const char *l, const char *r, Syntax syntax) {
  int i;
  switch (syntax) {
  case C: case LUA: case PYTHON:
    for (i = 1; r - i >= l && r[-i] == '\\'; ++i)
      ;
    if (--i % 2 == 0)
      return 1;
  }
  return 0;
}

int strclos(const char *l, const char *r, const char *tok, Syntax syntax) {
  switch (syntax) {
  case C:
    if (*r == *tok && unescaped(l, r, syntax))
        return 1;
    break;
  case LUA:
    if (*r == *tok && unescaped(l, r, syntax))
        return 1;
    if (r[0] == ']' && strncmp(tok, "--[", 3) == 0) {
      int i = 1;
      while (r[i] == '=' && tok[2+i] == '=')
        ++i;
      if (r[i] == ']')
        return i+1;
    }
    break;
  case PYTHON: /* no block comments in Python */
    break;
  }
  return -1;
}

int lcopen(const char *s, Syntax syntax) {
  switch (syntax) {
  case C:
    if (s[0] == '/' && s[1] == '/')
        return 2;
    break;
  case LUA:
    if (strncmp(s, "--", 2) == 0 && s[2] != '[')
        return 2;
    break;
  case PYTHON: /* no block comments in Python */
    break;
  }
  return -1;
}

State sloc_line(char *s, State state, Syntax syntax, char *tok, size_t toksz) {
  int offset;
  char *q = s, *p = state == BLOCKCOMM ? s-1 : s;

  for (;;) {
    if (state != BLOCKCOMM && (*p == '\0' || *p == '\n')) {
      print(s, p);
      return state;
    }
    switch (state) {
    case BLOCKCOMM:
      /* consume q, to REG on comment closure */
      if (*q == '\0' || *q == '\n') {
        print(s, p);
        return state;
      }
      if ((offset = bcclos(q, tok, syntax)) != -1) {
        print(s, p);
        s = p = q + offset;
        state = REG;
      } else {
        ++q;
      }
      break;
    case STR:
      /* consume p, to REG on str closure */
      if (*p == '\0' || *p == '\n') {
        print(s, p);
        return state;
      }
      if ((offset = strclos(s, p, tok, syntax)) != -1) {
        p += offset;
        state = REG;
      } else {
        ++p;
      }
      break;
    case REG:
      /* consume p, change state with lookahead */
      if (lcopen(p, syntax) != -1) { /* line comment, ignore the rest of the line */
        --p;
        print(s, p);
        return REG;
      }
      if (bcopen(tok, toksz, p, syntax) != NULL) {
        q = p + strlen(tok);
        --p;
        state = BLOCKCOMM;
      } else if (stropen(tok, toksz, p, syntax) != NULL) {
        p += strlen(tok);
        state = STR;
      } else {
        ++p;
      }
      break;
    }
  }
}

void sloc_file(FILE *fp, Syntax syntax) {
  State state = REG;
  char *s, buf[0x200] = {'\0'}, tok[0x20] = {'\0'};
  while ((s = fgets(buf, sizeof(buf), fp)) != NULL) {
    state = sloc_line(s, state, syntax, tok, sizeof(tok));
  }
}

/*****************************************************************************/

int main(int argc, char **argv) {
  int i;
  Syntax syntax = C;
  for (i = 1; i < argc && argv[i][0] == '-'; ++i) {
    switch (argv[i][1]) {
    case 'h':
      usage(0);
    case 's':
      if (++i < argc) {
        if (strcmp(argv[i], "c") == 0) {
          syntax = C;
          break;
        } else if (strcmp(argv[i], "lua") == 0) {
          syntax = LUA;
          break;
        } else if (strcmp(argv[i], "python") == 0) {
          syntax = PYTHON;
          break;
        }
      }
      usage(1);
    default:
      error(0, "unknown option: %s", argv[i]);
      usage(1);
    }
  }
  if (i >= argc)
    sloc_file(stdin, syntax);
  else
    for (; i < argc; ++i) {
      FILE *fp = fopen(argv[i], "r");
      if (fp == NULL)
        error(-1, "cannot open `%s':", argv[i]);
      sloc_file(fp, syntax);
      fclose(fp);
    }
  return 0;
}
