/* die roller by skog
 * this source code is distributed under MIT license
 * https://opensource.org/licenses/MIT
 */
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

const char *progname = "d6";

static int A = 1; /* number of dice to roll */
static int X; /* number of sides of the die */
static int H; /* keep only H highest rolls if >0 */
static int L; /* keep only L lowest rolls if >0 */
static int B; /* roll modifier; */
static int B_die; /* roll die mod, -1, 1 for lowest, -2, 2 for highest */
static int verbose = 0; /* show the reasult of each roll */

static int parse_roll(const char *notation);
static int roll(int a, int x, int keep_h, int keep_l, int b, int b_die);
static void error(int fatal, char *err, ...);
void *ealloc(size_t nbytes);

static void usage() {
  fprintf(stderr,
          "Usage: %s [options] [A]dX[h|lY][+B]\n"
          "Roll A X-sided dice with optional modifier B added.\n"
          "Keep Y highest (hY) or lowest (lY) dice.\n"
          "B can be `L' or `H' for lowest or highest single die in the roll.\n"
          "Ommitting A rolls one die.\n"
          "\nRoll notation examples:\n"
          "  d20      roll 1 20-sided die\n"
          "  3d4+2    roll 3 4-sided dice, add 2 to the result\n"
          "  4d6-L    roll 4 6-sided dice, subtract the lowest roll\n"
          "  6d6h4+3  roll 6 6-sided dice, keep 4 highest rolls, add 3\n"
          "\nOptions:\n"
          "  -h         show this message and exit\n"
          "  -s <seed>  seed the RNG with <seed> parsed as integer\n"
          "  -v         verbose: output details about the roll\n",
          progname);
  exit(0);
}

int main(int argc, char **argv) {
  int i, j, r;
  srand(time(NULL));

  for (i = 1; i < argc; ++i) {
    char *cur_argv = argv[i];
    if (cur_argv[0] != '-' || cur_argv[1] == '-')
      break;
    for (j = 1; cur_argv[j] != '\0'; ++j) {
      char *seedstr;
      long seed;
      switch (cur_argv[j]) {
      case 'h':
        usage();
      case 's':
        if (cur_argv[j+1] == '\0') {
          if (++i == argc)
            error(-1, "option `%c' requires a value", cur_argv[j]);
          seedstr = argv[i];
        } else {
          seedstr = cur_argv + j + 1;
        }
        seed = atol(seedstr);
        if (!seed)
          error(-1, "invalid seed `%s'", seedstr);
        srand(seed);
        break;
      case 'v':
        ++verbose;
        break;
      default:
        error(-1, "unknown option `%c', -h for help", cur_argv[j]);
      }
    }
  }

  if (i == argc)
    usage();

  if (parse_roll(argv[i]) != 0)
    error(-1, "bad roll notation: `%s'", argv[i]);

  r = roll(A, X, H, L, B, B_die);
  printf("%d\n", r);

  return r;
}

#define info(...) do{if(verbose) fprintf(stderr,__VA_ARGS__);}while(0)
static int roll(int a, int x, int keep_h, int keep_l, int b, int b_die) {
  int *rolls = ealloc(a * sizeof(*rolls));
  int i, sign = b < 0 ? '-' : '+', absb = b < 0 ? -b : b, sum = 0;
  extern int intcmp(const void *, const void *);

  info("%dd%d: ", a, x);
  for (i = 0; i < a; ++i) {
    rolls[i] = rand() % x + 1;
    sum += rolls[i];
    info("%s%d", i == 0 ? "" : " + ", rolls[i]);
  }
  info(" = %d\n", sum);

  qsort(rolls, a, sizeof(rolls[0]), intcmp);

  if (keep_h > 0 || keep_l > 0) {
    int start = 0, end = a;
    sum = 0;
    info("%c%d: ", keep_h ? 'h' : 'l', keep_h ? keep_h : keep_l);
    if (keep_h)
      start = a - keep_h;
    else
      end = keep_l;
    for (i = start; i < a; ++i) {
      info("%s%d", i == start ? "" : " + ", rolls[i]);
      sum += rolls[i];
    }
    info(" = %d\n", sum);
  }

  if (b_die == 0)
    info("%c%d: %d ", sign, absb, sum);
  else {
    sign = '+';
    b = (b_die == -1 || b_die == 1) ? rolls[0] : rolls[a-1];
    absb = b;
    if (b_die < 0) {
      sign = '-';
      b = -b;
    }
    info("%c%c: %d ", sign, b_die == 1 || b_die == -1 ? 'L' : 'H', sum);
  }
  sum += b;
  info("%c %d = %d\n", sign, absb, sum);

  free(rolls);

  return sum;
}
#undef info

int intcmp(const void *p, const void *q) {
  int a = *(int *) p, b = *(int *) q;
  if (a < b)
    return -1;
  else if (a == b)
    return 0;
  else
    return 1;
}

int parse_roll(const char *nota) {
  int num;
  const char *p = nota;
  extern int parse_number(const char *s, int *n);

  p += parse_number(nota, &num);
  if (*p != 'd' && *p != 'D')
    return -1;
  if (num != 0)
    A = num;

  ++p;
  p += parse_number(p, &num);
  if (num == 0)
    return -1;
  X = num;
  if (*p == '\0')
    return 0;

  if (*p == 'h' || *p == 'H' || *p == 'l' || *p == 'L') {
    int n, high = (*p == 'h' || *p == 'H');
    ++p;
    p += parse_number(p, &num);
    if (num == 0)
      num = 1;
    if (high)
      H = num;
    else
      L = num;
  }

  if (*p == '+' || *p == '-') {
    int n, sign = *p == '+' ? 1 : -1;
    ++p;
    if (*p == 'h' || *p == 'H') {
      B_die = sign * 2;
      ++p;
    } else if (*p == 'l' || *p == 'L') {
      B_die = sign;
      ++p;
    } else {
      n = parse_number(p, &num);
      if (n == 0)
        return -1;
      p += n;
      B = num;
    }
  }

  return *p == '\0' ? 0 : -1;
}

int parse_number(const char *s, int *n) {
  int i;
  *n = 0;
  for (i = 0; s[i] >= '0' && s[i] <= '9'; ++i)
    *n = *n * 10 + s[i] - '0';
  return i;
}

void error(int fatal, char *err, ...) {
  va_list args;
  va_start(args, err);
  if (progname)
    fprintf(stderr, "%s: ", progname);
  vfprintf(stderr, err, args);
  va_end(args);
  if (err[strlen(err) - 1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  if (fatal)
    exit(fatal);
}

void *ealloc(size_t nbytes) {
  void *p = calloc(nbytes, 1);
  if (p == NULL)
    error(-1, "cannot allocate %zd bytes:", nbytes);
  return p;
}
