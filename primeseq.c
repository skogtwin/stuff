/* eratosthenes' sieve implementation by skog
 * this source code is distributed under MIT license:
 * https://opensource.org/licenses/MIT
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

const char *progname = "primeseq";
const char *helpmsg = "usage: %s N\n"
                      "shows first N primes\n";

static uint8_t *sieve;

#define getbit(n) (sieve[(n)/8] & ((1 << (n)%8)))
#define setbit(n) (sieve[(n)/8] |= ((1 << (n)%8)))

void usage() {
  fprintf(stderr, helpmsg, progname);
  exit(1);
}

void fillsieve(unsigned long nlimit, unsigned long memlimit) {
  unsigned long long i, j, n = 1;
  unsigned long long step, bitlimit = memlimit*8;
  unsigned long limit = sqrt(bitlimit);
  printf("%d\n", 2);
  for (i = 1; i <= bitlimit; ++i) {
    if (!getbit(i)) {
      step = i*2 + 1;
      printf("%llu\n", step);
      if (++n >= nlimit)
        break;
      if (i >= limit)
        continue;
      for (j = step * step / 2; j < bitlimit; j += step)
        setbit(j);
    }
  }
  if (n < nlimit)
    fprintf(stderr, "%s: memory limit reached\n", progname);
}

int main(int argc, char **argv) {
  unsigned long n, mem;

  if (argc < 2 || (n = atol(argv[1])) <= 0)
    usage();
  mem = n*log(n);
  sieve = malloc(mem);
  if (sieve == NULL) {
    fprintf(stderr, "%s: not enough memory\n", progname);
    exit(1);
  }
  memset(sieve, '\0', mem);
  fillsieve(n, mem);
  return 0;
}
