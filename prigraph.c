/* Distributed under MIT license, no warranty, use on your own risk. */
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define nelem(x) (sizeof (x) / sizeof *(x))

const char *progname = "prigraph";
void usage() {
  fprintf(stderr, "Usage: %s [options]\n", progname);
  fprintf(stderr, "Draws the ASCII graph from TGF description read from stdin.\n"
          "Options are:\n"
          "  -w <width>  limit output to <width> characters if possible\n"
          "  -h          print this message and exit\n");
  exit(1);
}

static int output_width = 48;

typedef struct _Layer {
  int sz;     /* the number of nodes the layer holds */
  int capa;   /* the number of nodes the layer can fit */
  int *nodes; /* node's indices */
} Layer;

typedef struct _Node {
  char *name;
  int esz;    /* number of incident edges */
  int ecapa;  /* capacity of the child nodes' vector */
  int *nodes; /* vector of child nodes' indices */
  int layer;
} Node;

static Node **nodes;  /* dynamic array of pointers to the nodes */
static int nnodes;    /* nodes array size */
static Layer *layers; /* dynamic array that maps node's indices to layers */
static int nlayers;   /* size of the layer's array */


/*
 * utility functions
 */
int error(int fatal, char *err, ...) {
  va_list args;
  va_start(args, err);
  if (progname)
    fprintf(stderr, "%s: ", progname);
  vfprintf(stderr, err, args);
  va_end(args);
  if (err[strlen(err) - 1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  if (fatal)
    exit(1);
  return fatal;
}

void *ealloc(size_t nbytes) {
  void *p = calloc(nbytes, 1);
  if (p == NULL)
    error(1, "Cannot allocate %zd bytes:", nbytes);
  return p;
}

void *erealloc(void *p, size_t nbytes) {
  void *p2 = realloc(p, nbytes);
  if (p2 == NULL)
    error(1, "Cannot re-allocate %zd bytes:", nbytes);
  return p2;
}

char *estrdup(const char *s) {
  char *t = ealloc(strlen(s) + 1);
  return strcpy(t, s);
}

void rtrim(char *s) {
  int len = strlen(s);
  while (isspace(s[len - 1]))
    s[--len] = '\0';
}


/*
 * parse tgf
 */
void parse_node(char *s, int *n, char **t) {
  *n = -1;
  while (*s != '\0' && isspace(*s))
    ++s;
  if (!isdigit(*s))
    return;
  *n = 0;
  while (*s != '\0' && isdigit(*s))
    *n = *n * 10 + *s++ - '0';
  if (*s != '\0' && !isspace(*s)) {
    *n = -1;
    return;
  }
  while (*s != '\0' && isspace(*s))
    ++s;
  *t = *s == '\0' ? NULL : s;
}

Node *create_node(char *name) {
  Node *n = ealloc(sizeof *n);
  if (name != NULL)
    n->name = estrdup(name);
  return n;
}

void add_node(int at, char *s) {
  if (nnodes == 0) {
    nodes = ealloc((at + 1) * sizeof *nodes);
    nnodes = at + 1;
  } else if (at + 1 > nnodes) {
    nodes = erealloc(nodes, (at + 1) * sizeof *nodes);
    nnodes = at + 1;
  }
  nodes[at] = create_node(s);
}

void parse_nodes(FILE *fp) {
  char buf[0x400], *p;
  while (fgets(buf, sizeof buf, fp) != NULL && buf[0] != '#') {
    int ndno;
    rtrim(buf);
    parse_node(buf, &ndno, &p);
    if (ndno >= 0)
      add_node(ndno, p);
  }
}

void parse_edge(char *s, int *from, int *to) {
  int n;
  *from = *to = -1;

  while (*s != '\0' && isspace(*s))
    ++s;

  if (!isdigit(*s))
    return;

  n = 0;
  while (*s != '\0' && isdigit(*s))
    n = n * 10 + *s++ - '0';
  if (*s != '\0' && !isspace(*s))
    return;
  *from = n;

  while (*s != '\0' && isspace(*s))
    ++s;
  if (!isdigit(*s))
    return;

  n = 0;
  while (*s != '\0' && isdigit(*s))
    n = n * 10 + *s++ - '0';
  *to = n;

  if (*from > *to) {
    n = *from;
    *from = *to;
    *to = n;
  }
}

void grow_edges(Node *n) {
  if (n->ecapa == 0) {
    n->nodes = ealloc(sizeof *n->nodes);
    n->ecapa = 1;
  } else {
    n->ecapa *= 2;
    n->nodes = erealloc(n->nodes, n->ecapa * sizeof *n->nodes);
  }
}

void add_edge(int from, int to) {
  Node *n = nodes[from];
  if (n->ecapa == n->esz)
    grow_edges(n);
  n->nodes[n->esz++] = to;
}

void parse_edges(FILE *fp) {
  char buf[0x400];
  while (fgets(buf, sizeof buf, fp) != NULL) {
    int edgeno, to;
    rtrim(buf);
    parse_edge(buf, &edgeno, &to);
    if (edgeno >= 0 && to >= 0)
      add_edge(edgeno, to);
  }
}

int parse_tgf(FILE *fp) {
  parse_nodes(fp);
  parse_edges(fp);
  return nnodes != 0;
}


/*
 * build layered graph structure
 */

int longest2sink(int n) {
  int i, max = 1;
  for (i = 0; i < nodes[n]->esz; ++i) {
    int cur = 1 + longest2sink(nodes[n]->nodes[i]);
    if (cur > max)
      max = cur;
  }
  return max;
}

void add_layer(int no) {
  if (layers == NULL) {
    layers = ealloc(no * sizeof *layers);
    nlayers = no;
  } else if (nlayers < no) {
    layers = erealloc(layers, no * sizeof *layers);
    memset(layers + nlayers, 0, (no - nlayers) * sizeof *layers);
    nlayers = no;
  }
}

void grow_layer(Layer *l) {
  if (l->capa == 0) {
    l->nodes = ealloc(sizeof *l->nodes);
    l->capa = 1;
  } else if (l->capa >= l->sz) {
    l->capa <<= 1;
    l->nodes = realloc(l->nodes, sizeof *l->nodes * l->capa);
  }
}

void add2layer(int layerno, int nodeno) {
  Layer *l;
  add_layer(layerno);
  l = layers + layerno - 1;
  if (l->capa <= l->sz)
    grow_layer(l);
  l->nodes[l->sz++] = nodeno;
}

void connect_nodes(int n, int m) {
  int i, at, nlayers = nodes[n]->layer - nodes[m]->layer;
  if (nlayers == 1)
    return;
  at = nnodes;
  add_node(at, NULL);
  add_edge(at, m);
  nodes[at]->layer = nodes[n]->layer - 1;
  add2layer(nodes[at]->layer, at);
  for (i = 0; i < nodes[n]->esz; ++i)
    if (nodes[n]->nodes[i] == m)
      nodes[n]->nodes[i] = at;
  connect_nodes(at, m);
}

void place_fake_nodes(int n) {
  int i;
  for (i = 0; i < nodes[n]->esz; ++i)
    connect_nodes(n, nodes[n]->nodes[i]);
}

void assign_layers() {
  int i, nlayers = 0;
  /* all sinks are lvl 1 */
  for (i = 0; i < nnodes; ++i)
    if (nodes[i] && nodes[i]->esz == 0) {
      nodes[i]->layer = 1;
      add2layer(1, i);
    }
  /* other nodes' layer equals longest path to sink */
  for (i = 0; i < nnodes; ++i)
    if (nodes[i] && nodes[i]->esz) {
      int layer = longest2sink(i);
      nodes[i]->layer = layer;
      add2layer(layer, i);
    }
  /* make sure no edge pass through a layer by putting fake nodes */
  for (i = 0; i < nnodes; ++i)
    if (nodes[i] && nodes[i]->esz)
      place_fake_nodes(i);
}

int layer_size(int n) {
  return layers[n-1].sz;
}

int max_layer_size() {
  int i, sz, max = 0;
  for (i = 1; i <= nlayers; ++i) {
    sz = layer_size(i);
    if (max < sz)
      max = sz;
  }
  return max;
}

int weight(int n) {
  int i, j, total = 0, weight = 0, layer = nodes[n]->layer - 2;
  for (i = 0; i < layers[layer].sz; ++i)
    for (j = 0; j < nodes[n]->esz; ++j)
      if (layers[layer].nodes[i] == nodes[n]->nodes[j]) {
        total++;
        weight += i;
      }
  return total ? weight / total : 0;
}

int nodecmp(const void *a, const void *b) {
  int x = *(int *) a;
  int y = *(int *) b;
  int xw = weight(x);
  int yw = weight(y);
  return xw - yw;
}

void reduce_edge_crossing() {
  int i;
  for (i = 1; i < nlayers; ++i)
    qsort(layers[i].nodes, layers[i].sz, sizeof layers[i].nodes[0], nodecmp);
}

void build_graph() {
  assign_layers();
  reduce_edge_crossing();
}


/*
 * draw graph
 */

static struct {
  int nodeid;
  int x;
} start_points[0x200], end_points[0x200]; /* beginnings and ends of the edges */
static int nstarts, nends;                /* # of elements in above arrays */

void add_edge_startpoint(int x, int nodeid) {
  int i = nstarts++;
  if (i == nelem(start_points))
    error(1, "Too many nodes on a level (%d)", i);
  start_points[i].nodeid = nodeid;
  start_points[i].x = x;
}

void add_edge_endpoint(int x, int nodeid) {
  int i = nends++;
  if (i == nelem(end_points))
    error(1, "Too many nodes on a level (%d)", i);
  end_points[i].nodeid = nodeid;
  end_points[i].x = x;
}

void shift_edge_points() {
  memmove(start_points, end_points, sizeof end_points);
  nstarts = nends;
  nends = 0;
}

int layer_width(int layer) {
  int i, width = 0;
  for (i = 0; i < layers[layer].sz; ++i) {
    Node *n = nodes[layers[layer].nodes[i]];
    width += n->name ? strlen(n->name) : 1;
  }
  return width;
}

int layer_max_width() {
  int i, w, max = 0, widest = 0;
  for (i = 0; i < nlayers; ++i) {
    w = layer_width(i);
    if (w > max) {
      max = w;
      widest = i;
    }
  }
  return max + (layers[widest].sz - 1); /* a space between nodes */
}

static char **new_buffer() {
  char **buffer = ealloc(sizeof(int) + 2 * sizeof *buffer);
  *(int *) buffer = 1; /* capacity */
  buffer = (char **) ((int *) buffer + 1);
  *buffer = ealloc(output_width + 1);
  memset(*buffer, ' ', output_width);
  return buffer;
}

static void free_buffer(char **buffer) {
  buffer = (char **) ((int *) buffer - 1);
  free(buffer);
}

static int buffer_len(char **buffer) {
  return *((int *) buffer - 1);
}

static char **buffer_alloc_lines(char **buffer, int nlines) {
  int i, prev_len = buffer_len(buffer);
  buffer = (char **) ((int *) buffer - 1);
  buffer = erealloc(buffer, sizeof(int) + (nlines + 1) * sizeof *buffer);
  *(int *) buffer = nlines;
  buffer = (char **) ((int *) buffer + 1);
  buffer[nlines] = NULL;
  for (i = prev_len; i < nlines; ++i) {
    buffer[i] = ealloc(output_width + 1);
    memset(buffer[i], ' ', output_width);
  }
  return buffer;
}

static char **buffer_setxy(char **buffer, int col, int row, char c) {
  if (buffer_len(buffer) <= row)
    buffer = buffer_alloc_lines(buffer, row + 1);
  if (col >= output_width)
    error(1, "Put char outside the buffer boundaries (%d, %d)", col, row);
  buffer[row][col] = c;
  return buffer;
}

static int buffer_getxy(char **buffer, int col, int row) {
  if (row >= buffer_len(buffer) || buffer[row] == NULL)
    return -1;
  return buffer[row][col];
}

char **draw_edge(int from, int to, char **buffer) {
  int x = from;
  int y = 0;
  int step = to - from > 0 ? 1 : -1;
  int vertical = 0; /* do we move horizontally, or vertically */
  buffer = buffer_setxy(buffer, x, y, '|');
  buffer = buffer_setxy(buffer, x, ++y, '+');
  while (x != to) {
    if (buffer_getxy(buffer, x + step, y) != ' '
        && buffer_getxy(buffer, x, y) == ' ') {
      buffer = buffer_setxy(buffer, x, y, '+');
      y++;
      buffer = buffer_setxy(buffer, x, y, '|');
      vertical = 1;
    } else {
      int c;
      x += step;
      /* only print something if there's no beaten track */
      if (buffer_getxy(buffer, x, y) == ' ')
        buffer = buffer_setxy(buffer, x, y, vertical ? '+' : '-');
      vertical = 0;
    }
  }
  buffer = buffer_setxy(buffer, x, y, '+');
  buffer = buffer_setxy(buffer, x, ++y, '|');
  return buffer;
}

void draw_edges() {
  int i, j, k;
  char **p, **buffer = new_buffer(); /* piece of screen representation */
  Node *n;
  for (i = 0; i < nstarts; ++i) {
    n = nodes[start_points[i].nodeid];
    for (j = 0; j < nends; ++j) {
      int nodeid = end_points[j].nodeid;
      for (k = 0; k < n->esz; ++k)
        if (n->nodes[k] == nodeid)
          break;
      if (k != n->esz) /* found */
        buffer = draw_edge(start_points[i].x, end_points[j].x, buffer);
    }
  }
  for (p = buffer; *p != NULL; ++p) {
    printf("%s\n", *p);
    free(*p);
  }
  free_buffer(buffer);
}

void print_graph() {
  Node *n;
  int i, j, k, curx, len, pad;
  int layer_max_w = layer_max_width();

  if (layer_max_w > output_width)
    output_width = layer_max_w;

  /* center first layer (the one with greates id); two spaces between nodes */
  pad = output_width - layer_width(nlayers-1) - (layers[nlayers-1].sz - 1) * 2;
  pad /= 2;
  for (k = 0; k < pad; ++k)
    fputc(' ', stdout);
  curx = pad;
  for (j = 0; j < layers[nlayers-1].sz; ++j) {
    char *name, *spacing;
    int dx;
    n = nodes[layers[nlayers-1].nodes[j]];
    name = n->name ? n->name : "|";
    spacing = (j+1 == layers[nlayers-1].sz) ? "" : "  ";
    printf("%s%s", name, spacing);
    dx = strlen(name);
    add_edge_startpoint(curx + dx/2, layers[nlayers-1].nodes[j]);
    curx += dx + strlen(spacing);
  }
  fputc('\n', stdout);

  /* full justify the rest of the layers */
  for (i = nlayers - 2; i >= 0; --i) {
    int curx = 0;
    int layer_w = layer_width(i);
    pad = output_width - layer_w;
    if (layers[i].sz > 1)
      pad /= layers[i].sz - 1;

    /* precalculate the endings of edges */ 
    for (j = 0; j < layers[i].sz; ++j) {
      int dx;
      if (j)
        curx += pad;
      else if (layers[i].sz == 1)
        curx += pad/2 + pad%2;
      n = nodes[layers[i].nodes[j]];
      dx = n->name ? strlen(n->name) : 1;
      add_edge_endpoint(curx + dx/2, layers[i].nodes[j]);
      curx += dx;
    }

    draw_edges();
    shift_edge_points(); /* make the edges' ends the new edges' beginnings */

    /* actually output nodes */
    for (j = 0; j < layers[i].sz; ++j) {
      if (j)
        for (k = 0; k < pad; ++k)
          fputc(' ', stdout);
      else if (layers[i].sz == 1)
        for (k = 0; k < pad/2 + pad%2; ++k)
          fputc(' ', stdout);
      n = nodes[layers[i].nodes[j]];
      printf("%s", n->name ? n->name : "|");
    }
    fputc('\n', stdout);
  }
}


/*
 * connect pieces
 */
int main(int argc, char **argv) {
  int i;
  for (i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-w") == 0) {
      if (argv[++i] == NULL) {
        error(0, "No width specified for `-w' option\n");
        usage();
      }
      output_width = atoi(argv[i]);
    } else if (strcmp(argv[i], "-h") == 0) {
      usage();
    } else {
      error(0, "Uknown option `%s'\n", argv[i]);
      usage();
    }
  }
  if (parse_tgf(stdin)) {
    build_graph();
    print_graph();
  }
  return 0;
}
