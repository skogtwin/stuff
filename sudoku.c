/* simple sudoku solver by skog
 * this source code is distributed under MIT license
 * https://opensource.org/licenses/MIT
 */
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


#define BOARDSZ 9
#define NCELLS (BOARDSZ*BOARDSZ)


const char *progname = "sudoku";

static int print_boards = 1; /* print solved board(s) */
static int max_solutions = 100; /* stop solving after finding this many solutions */

static void usage() {
  const char *usage =
"Usage: %s [options] [file...]\n"
"Solves a sudoku puzzle. Fills in the board with a series of digits from the\n"
"input, left-to-right, top-to-bottom; 0 means empty cell, non-digit characters\n"
"are ignored.\n"
"Options are:\n"
"  -n <num>  stop after finding <num> solutions; default is %d, -1 disables\n"
"  -q        do not print solved boards, only the number of solutions found\n"
"  -h        print this message and exit\n";
  fprintf(stderr, usage, progname, max_solutions);
  exit(-1);
}


static char board[NCELLS]; /* sudoku board */
static struct {
  char **boards; /* array of solved sudoku boards */
  int sz, cap; /* size, capacity */
} solutions; /* found solutions */


int set_flag(int c, const char *v); /* set a commandline option; TRUE if argc should be advanced */

const char *load(FILE *fp); /* load the board from file */
int check_board(const char **result); /* check if the board can be solved */
int solve(); /* solve the current board */
int try_moves(int to); /* try all possible moves (1-9) at particular cell */
int can_move(int to, int with); /* 0 unless 'with' is a legal number in cell 'to' */
void add_solution(); /* add a solved board to solutions.boards */
void clear_solutions(); /* clears found solutions, frees the memory */
void print_solutions(FILE *fp, const char *name); /* print found solutions */
void print_solved_boards(FILE *fp); /* print solutions.boards */

int error(int fatal, char *err, ...); /* report error, terminate if fatal != 0 */
void *ealloc(size_t nbytes); /* allocate 'nbytes' bytes, terminate on failure */
void *erealloc(void *p, size_t nbytes); /* re-allocate 'p' to 'nbytes'; terminate on failure */


int sudoku(FILE *in, const char *board_name, FILE *out) {
  int check;
  const char *err;
  if ((err = load(in)) != NULL) {
    error(0, "could not load board: %s", err);
    return -1;
  }
  if (board_name != NULL)
    fprintf(out, "%s: ", board_name);
  if ((check = check_board(&err)) != 0) {
    fprintf(out, "%s\n", err);
    return check;
  }
  if (!solve()) {
    fprintf(out, "no solution found\n");
    return 1;
  }
  print_solutions(out, board_name);
  return 0;
}


int main(int argc, char **argv) {
  FILE *fp;
  int i, j;
  for (i = 1; i < argc; ++i) {
    if (argv[i][0] != '-')
      break;
    for (j = 1; argv[i][j] != '\0'; ++j) {
      if (set_flag(argv[i][j], argv[i+1])) {
        ++i;
        break;
      }
    }
  }
  if (i == argc)
    return sudoku(stdin, NULL, stdout);
  for (; i < argc; ++i) {
    if ((fp = fopen(argv[i], "r")) == NULL) {
      error(0, "could not open `%s':", argv[i]);
      continue;
    }
    sudoku(fp, argv[i], stdout);
    fclose(fp);
    clear_solutions();
  }
  return 0;
}


int set_flag(int c, const char *v) {
  switch (c) {
  case 'h':
    usage();
  case 'q':
    print_boards = 0;
    return 0;
  case 'n':
    if (v == NULL)
      error(-1, "option `%c' requires a value", c);
    max_solutions = atoi(v);
    if (max_solutions == 0)
      error(-1, "invalid value for option `%c': %s", c, v);
    return 1;
  default:
    error(0, "unknown option `%c'", c);
    usage();
  }
  return 0; /* unreachable */
}


const char *load(FILE *fp) {
  int i, c;
  static char err[0x200] = {0};
  for (i = 0; i < NCELLS; ++i) {
    while ((c = fgetc(fp)) != EOF)
      if (c >= '0' && c <= '9')
        break;
    if (c == EOF)
      return "not enough digits in input";
    board[i] = c - '0';
  }
  return NULL;
}


#define inset(set, bit) (set & (1 << (bit-1)))
#define set(set, bit) do { (set |= (1 << (bit-1))); } while (0)

int check_board(const char **result) {
  int i, j, full = 1;
  static char msg[0x200] = {0};

  /* check columns and rows */
  for (i = 0; i < BOARDSZ; ++i) {
    int rset = 0, cset = 0;
    for (j = 0; j < BOARDSZ; ++j) {
      int cr = board[i*BOARDSZ + j];
      int cc = board[j*BOARDSZ + i];
      if (cr && inset(rset, cr)) {
        sprintf(msg, "duplicate digit %d at row %d", cr, i/BOARDSZ + 1);
        *result = msg;
        return -1;
      } else if (cr)
        set(rset, cr);
      if (cc && inset(cset, cc)) {
        sprintf(msg, "duplicate digit %d at column %d", cc, j+1);
        *result = msg;
        return -1;
      } else if (cc)
        set(cset, cc);
      if (cr == 0 || cc == 0)
        full = 0;
    }
  }
  /* check squares (is it really needed?) */
  for (i = 0; i < BOARDSZ; i += 3) {
    for (j = 0; j < BOARDSZ; j += 3) {
      int di, dj, sset = 0;
      for (di = 0; di < 3; ++di) {
        for (dj = 0; dj < 3; ++dj) {
          int cs = board[(i+di)*BOARDSZ + j+dj];
          if (cs && inset(sset, cs)) {
            sprintf(msg, "duplicate digit %d at square %d,%d", cs, i/3+1, j/3+1);
            *result = msg;
            return -1;
          } else if (cs) {
            set(sset, cs);
          } else
            full = 0;
        }
      }
    }
  }

  if (full)
    *result = "solved";
  return full;
}

int solve() {
  int i;
  for (i = 0; i < NCELLS; ++i)
    if (board[i] == 0)
      return try_moves(i);
  add_solution();
  return 1;
}

int try_moves(int to) {
  int i, res, n = 0;
  for (i = 1; i <= 9; ++i) {
    if (!can_move(to, i))
      continue;
    board[to] = i;
    n += solve();
    board[to] = 0;
    if (max_solutions > 0 && solutions.sz >= max_solutions)
      return n;
  }
  return n;
}

int can_move(int to, int with) {
  int i, j;
  int row = to / BOARDSZ;
  int col = to % BOARDSZ;
  int srow = row / 3 * 3;
  int scol = col / 3 * 3;

  /* check row */
  for (i = 0; i < BOARDSZ; ++i)
    if (board[row*BOARDSZ + i] == with)
      return 0;

  /* check column */
  for (i = 0; i < BOARDSZ; ++i)
    if (board[i*BOARDSZ + col] == with)
      return 0;

  /* check square */
  for (i = srow; i < srow + 3; ++i)
    for (j = scol; j < scol + 3; ++j)
      if (board[i*BOARDSZ + j] == with)
        return 0;

  return 1;
}

void add_solution() {
  if (solutions.cap == 0) {
    solutions.cap = 1;
    solutions.boards = ealloc(sizeof(solutions.boards[0]));
  } else if (solutions.sz == solutions.cap) {
    solutions.cap *= 2;
    solutions.boards = erealloc(solutions.boards,
                                solutions.cap * sizeof(solutions.boards));
  }
  solutions.boards[solutions.sz] = ealloc(NCELLS);
  memcpy(solutions.boards[solutions.sz], board, NCELLS);
  ++solutions.sz;
}


void clear_solutions() {
  int i;
  for (i = 0; i < solutions.sz; ++i)
    free(solutions.boards[i]);
  free(solutions.boards);
  solutions.sz = solutions.cap = 0;
}

void print_solutions(FILE *fp, const char *name) {
  if (name != NULL)
    fprintf(fp, "%s: ", name);
  fprintf(fp, "%d solution%s\n", solutions.sz, solutions.sz == 1 ? "" : "s");
  if (print_boards)
    print_solved_boards(fp);
}

void print_solved_boards(FILE *fp) {
  int i, j, n;
  for (n = 0; n < solutions.sz; ++n) {
    char *boardp = solutions.boards[n];
    for (i = 0; i < BOARDSZ; ++i) {
      for (j = 0; j < BOARDSZ; ++j) {
        const char *sep = (j == 0 ? "" : (j%3 == 0 ? "  " : " "));
        fprintf(fp, "%s%d", sep, (int) boardp[BOARDSZ*i + j]);
      }
      fputc('\n', fp);
      if (i+1 < BOARDSZ && (i+1)%3 == 0)
        fputc('\n', fp);
    }
    if (n + 1 != solutions.sz)
      fputs("\n---\n\n", fp);
  }
}


int error(int fatal, char *err, ...) {
  va_list args;
  va_start(args, err);
  if (progname)
    fprintf(stderr, "%s: ", progname);
  vfprintf(stderr, err, args);
  va_end(args);
  if (err[strlen(err) - 1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  if (fatal)
    exit(1);
  return fatal;
}

void *ealloc(size_t nbytes) {
  void *p = calloc(nbytes, 1);
  if (p == NULL)
    error(1, "Cannot allocate %zd bytes:", nbytes);
  return p;
}

void *erealloc(void *p, size_t nbytes) {
  void *p2 = realloc(p, nbytes);
  if (p2 == NULL)
    error(1, "Cannot re-allocate %zd bytes:", nbytes);
  return p2;
}
