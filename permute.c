#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char *progname = "permute";
void usage() {
  fprintf(stderr, "Usage: %s SEQUENCE\n"
                  "   or: %s ELEMENT0 .. ELEMENTn\n"
                  "Prints all the combinations of ELEMENTS0 .. ELEMENTSn\n"
                  "or characters in SEQUENCE (if you patient enough ofcourse)\n",
          progname, progname);
  exit(1);
}

int smode = 0;

char **elements;
char **permutation;
int nelements;

int exists(char *n, int lim) {
  int i;
  for (i = 0; i < lim; ++i)
    if (permutation[i] == n)
      return 1;
  return 0;
}

void permute(int iter) {
  int i;
  if (iter == nelements) {
    for (i = 0; i < nelements; ++i)
      smode ? printf("%s ", permutation[i]) : printf("%c", *permutation[i]);
    fputs("\n", stdout);
    return;
  }
  for (i = 0; i < nelements; ++i) {
    char *n = elements[i];
    if (exists(n, iter))
      continue;
    permutation[iter] = n;
    permute(iter + 1);
  }
}

int main(int argc, char **argv) {
  int i;
  if (argc == 1)
    usage();
  smode = argc > 2;
  nelements = smode ? argc - 1 : strlen(argv[1]);
  elements = malloc(nelements * sizeof *elements);
  permutation = malloc(nelements * sizeof *elements);
  if (elements == NULL || permutation == NULL) {
    fprintf(stderr, "%s: not enough memory\n", progname);
    exit(-1);
  }
  if (argc == 2)
    for (i = 0; argv[1][i] != '\0'; ++i)
      elements[i] = &argv[1][i];
  else
    for (i = 1; i < argc; ++i)
      elements[i-1] = argv[i];
  permute(0);
  return 0;
}
