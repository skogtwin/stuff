/*
 * Conway's game of life for ANSI terminal by skogtwin.
 * No Windows support.
 * Distributed under MIT license (http://opensource.org/licenses/MIT)
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>


static const char *progname = "gol";

static int width = 80, height = 24; /* viewport size */
static int fps = 5; /* frames per second */
static int showstatus; /* show status line on top */

void usage() {
  fprintf(stderr, "Usage: %s [OPTIONS]\n"
                  "options are:\n"
                  "  -width   viewport width\n"
                  "  -height  viewport height\n"
                  "  -fps     frames per second\n"
                  "  -status  show status line on top\n",
          progname);
  exit(1);
}

/* utility stuff */

int tty_width() {
  struct winsize w;
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &w) == -1)
    return 80;
  return w.ws_col;
}

int tty_height() {
  struct winsize w;
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &w) == -1)
    return 24;
  return w.ws_row;
}

int error(int fatal, char *err, ...) {
  va_list args;
  va_start(args, err);
  if (progname)
    fprintf(stderr, "%s: ", progname);
  vfprintf(stderr, err, args);
  va_end(args);
  if (err[strlen(err) - 1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  if (fatal)
    exit(1);
  return fatal;
}

void *ealloc(size_t nbytes) {
  void *p = calloc(nbytes, 1);
  if (p == NULL)
    error(1, "Cannot allocate %zd bytes:", nbytes);
  return p;
}

/* time_ms() returns some consistent timestamp in milliseconds. ****************
 * I know no real cross-platform way to implement it, so here be ifdefs.
 * Copypasted from fitmr.c
 */
#if defined(_POSIX_C_SOURCE) && _POSIX_C_SOURCE >= 199309L

uint64_t time_ms() {
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  return (uint64_t) now.tv_sec * 1000 + now.tv_nsec / 1000000;
}

#elif defined(__MACH__)

#include <mach/clock.h>
#include <mach/mach.h>

uint64_t time_ms() {
  clock_serv_t cclock;
  mach_timespec_t now;

  host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
  clock_get_time(cclock, &now);
  mach_port_deallocate(mach_task_self(), cclock);

  return (uint64_t) now.tv_sec * 1000 + now.tv_nsec / 1000000;
}

#else

uint64_t time_ms() {
  return (uint64_t) clock() * 1000 / CLOCKS_PER_SEC;
}

#endif
/*****************************************************************************/

/* game itself */

static char **field; /* 2d dynamic array,
                        ' ' for empty, '.' for newborn, '!' for dying,
                        '#' for adult */
static int maxx, maxh; /* starting dimensions, used for centering start
                          position on screen */
static int turn; /* current move */
static int ncells; /* total cells on board */

void drawfield() {
  int y, x;
  fputs("\x1b[2J\x1b[1;1H", stdout);
  if (showstatus)
    printf("%d x %d @ %d fps | generation: %d | population: %d\n",
           width, height, fps, turn, ncells);
  for (y = 0; field[y] != NULL;  ++y) {
    for (x = 0; field[y][x] != '\0'; ++x)
      putc(field[y][x], stdout);
    putc('\n', stdout);
  }
  fflush(stdout);
}

int celltaken(int y, int x) {
  int ret;
  if (y < 0 || y >= height || x < 0 || x >= width)
    return 0;
  return field[y][x] == '#' || field[y][x] == '!';
}

int nneighbours(int y, int x) {
  int dy, dx, n = 0;
  for (dy = -1; dy < 2; ++dy)
    for (dx = -1; dx < 2; ++dx)
      if (dx != 0 || dy != 0)
        n += celltaken(y + dy, x + dx);
  return n;
}

void apply(void (*f)(int y, int x)) {
  int y, x;
  for (y = 0; field[y] != NULL; ++y)
    for (x = 0; field[y][x] != '\0'; ++x)
      f(y, x);
}

int mkmove() {
  int y, x, nchanges = 0;
  /* mark */
  for (y = 0; field[y] != NULL; ++y)
    for (x = 0; field[y][x] != '\0'; ++x) {
      int n = nneighbours(y, x);
      if (field[y][x] == '#' && (n < 2 || n > 3)) { /* dying */
        field[y][x] = '!';
        nchanges++;
      } else if (n == 3 && field[y][x] != '#') { /* newborn */
        field[y][x] = '.';
        nchanges++;
      }
    }
  /* sweep */
  for (y = 0; field[y] != NULL; ++y)
    for (x = 0; field[y][x] != '\0'; ++x) {
      if (field[y][x] == '!') {
        field[y][x] = ' ';
        --ncells;
      }
      else if (field[y][x] == '.') {
        field[y][x] = '#';
        ++ncells;
      }
    }
  return nchanges;
}

int alive() {
  int y, x;
  for (y = 0; field[y] != NULL; ++y)
    for (x = 0; field[y][x] != '\0'; ++x)
      if (field[y][x] != ' ')
        return 1;
  return 0;
}

void center(int curh, int curw) {
  int i;
  int dw = (width - curw - 1) / 2;
  int dh = (height - curh - 1) / 2;
  if (dh > 0) {
    memmove(field + dh, field, (height - dh - 1) * sizeof(*field));
    for (i = 0; i < dh; ++i) {
      field[i] = ealloc((width + 1) * sizeof(**field));
      memset(field[i], ' ', width * sizeof(**field));
      field[i][width] = '\0';
    }
  }
  if (dw > 0)
    for (i = 0; i < height; ++i) {
      memmove(field[i] + dw, field[i], (width - dw - 1) * sizeof(**field));
      memset(field[i], ' ', dw);
    }
}

void ldfield(FILE *fp) {
  int c, maxx, maxy, x, y;
  maxx = maxy = x = y = 0;
  while ((c = getc(fp)) != EOF) {
    if (c == '\n') {
      if (y > maxy)
        maxy = y;
      x = 0;
      ++y;
      continue;
    }
    if (y < height && x < width) {
      field[y][x] = c;
      if (!isspace(c))
        ++ncells;
    }
    if (x > maxx)
      maxx = x;
    ++x;
  }
  center(maxy + 1, maxx + 1);
}

void cleanup() {
  fputs("\x1b[?25h", stdout);
  exit(0);
}

void init(int argc, char **argv) {
  int i;
  if (isatty(fileno(stdout))) {
    width = tty_width();
    height = tty_height() - 1;
  }
  for (i = 1; i < argc; ++i)
    if (strcmp(argv[i], "-width") == 0) {
      if (i + 1 < argc)
        width = atoi(argv[++i]);
    } else if (strcmp(argv[i], "-height") == 0) {
      if (i + 1 < argc)
        height = atoi(argv[++i]);
    } else if (strcmp(argv[i], "-fps") == 0) {
      if (i + 1 < argc)
        fps = atoi(argv[++i]);
    } else if (strcmp(argv[i], "-status") == 0) {
      showstatus = 1;
      --height; /* reserve a place for a status line */
    } else if (argv[i][1] == 'h') {
      usage();
    } else {
      error(0, "uknown option `%s'", argv[i]);
      usage();
    }
}

void hidecursor() {
  if (isatty(fileno(stdout))) {
    /* turn cursor off */
    fputs("\x1b[?25l", stdout);
    /* make sure we'll turn cursor on again */
    atexit(cleanup);
    signal(SIGINT, (void (*)(int)) cleanup);
  }
}

int main(int argc, char **argv) {
  int i, nchanges;
  uint64_t start, elapsed, delay;
  struct timespec req;
  init(argc, argv);
  field = ealloc((height + 1) * sizeof(*field));
  for (i = 0; i < height; ++i) {
    field[i] = ealloc((width + 1) * sizeof(**field));
    memset(field[i], ' ', (width) * sizeof(**field));
    field[i][width] = '\0';
  }
  ldfield(stdin);
  hidecursor();
  delay = 1000 / fps;
  start = time_ms();
  req.tv_sec = 0;
  do {
    drawfield();
    elapsed = time_ms() - start;
    req.tv_nsec = elapsed < delay ? (delay - elapsed)*1000000 : 0;
    if (isatty(fileno(stdout)))
      nanosleep(&req, NULL);
    start = time_ms();
    nchanges = mkmove();
    ++turn;
  } while (alive() && nchanges);
  return 0;
}
