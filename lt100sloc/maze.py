#!/usr/bin/python
import sys, random, collections

def new_maze(w, h):
    return [['#' for _ in xrange(2 * w + 1)] for _ in xrange(2 * h + 1)]
  
def dig_maze(maze, x, y):
    bt = collections.deque([(x, y)])
    while len(bt):
        x, y = bt.pop()
        for x1, y1 in random.sample([(-2, 0), (2, 0), (0, -2), (0, 2)], 4):
            try:
                if y + y1 > 0 and x + x1 > 0 and maze[y + y1][x + x1] == '#':
                    for dx in range(x1, 1) + range(0, x1 + 1):
                        maze[y][x + dx] = ' '
                    for dy in range(y1, 1) + range(0, y1 + 1):
                        maze[y + dy][x] = ' '
                    bt.extend([(x, y), (x+x1, y+y1)])
                    break
            except IndexError:
                pass
    return maze

if __name__ == '__main__':
    width, height = 25, 7 
    if len(sys.argv) == 3:
        width, height = int(sys.argv[1]), int(sys.argv[2])
    x, y = random.randrange(width), random.randrange(height)
    for s in dig_maze(new_maze(width, height), x * 2 + 1, y * 2 + 1):
        print(''.join(s))
