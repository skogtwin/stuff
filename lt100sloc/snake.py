#!/usr/bin/python
import curses
from curses import wrapper
import os, random, time

width, height = 50, 15
record, hero = 1, 'skog'
speed, scale = 10, (0.6, 1.0)
savepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'snake.hs')

def save_highscore(score, nickname):
    if not nickname:
        nickname = 'Anonymous'
    with open(savepath, "w") as f:
        print >>f, score, nickname

def print_score(cur):
    print "Game over.\nYou scored %d points." % cur
    if cur > int(record):
        save_highscore(cur, raw_input("New record! Type your name: "))
    else:
        print "The current record of %s points was set by %s." % (record, hero)

def draw_status(scr, lvl, score):
    scr.addstr(height, 0, "[Speed: %s] [Score: %s] [Record: %d by %s]"
                              % (lvl, score, int(record), hero))

def draw_objects(scr, *objects):
    for sym, at in objects:
        for y, x in at:
            scr.addch(y, x, sym)

def play(stdscr):
    stdscr.nodelay(True)
    curses.noecho()
    curses.cbreak()
    curses.curs_set(0)
    level, score = 1, 0
    borders = [(y, x) for x in xrange(width) for y in xrange(height)
                      if x in (0, width - 1) or y in (0, height - 1)]
    fruit = []
    snake = [(height/2, width/2), (height/2, width/2 + 1)]
    moves = { curses.KEY_UP: (-1, 0), curses.KEY_DOWN: (1, 0),
              curses.KEY_LEFT: (0, -1), curses.KEY_RIGHT: (0, 1) }
    curmove = (0, -1)
    draw_objects(stdscr, ('#', borders))
    while True:
        curframe = time.time()
        if not fruit:
            fruit = [(random.randint(1, height-2), random.randint(1, width-2))]
        where = moves.get(stdscr.getch(), None)
        if where:
            curmove = where
        snake.insert(0, (snake[0][0] + curmove[0], snake[0][1] + curmove[1]))
        if snake[0] in snake[1:] or snake[0] in borders:
            break;
        elif snake[0] in fruit:
            score += level
            level += (score >= (1 << level))
            shade, fruit = [], []
        else:
            shade = [snake.pop()]
        draw_objects(stdscr, ('@', fruit), ('*', snake), (' ', shade))
        draw_status(stdscr, level, score)
        nextframe = curframe + 1.0 / ((level + speed) * scale[curmove[0] == 0])
        if nextframe > time.time():
            time.sleep(nextframe - time.time())
    return score

if __name__ == '__main__':
    try:
        record, hero = open(savepath, "r").readline().rstrip('\n').split(' ')
    except:
        pass
    print_score(curses.wrapper(play))
