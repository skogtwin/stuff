#!/usr/bin/python
import sys, collections

maze = []
walls = '#|_-+='

def solve(start, finish):
  visit = collections.deque([start])
  came_from = {start: None}
  while len(visit) != 0:
    at = visit.popleft()
    for x, y in [(at[0],   at[1]+1), (at[0],   at[1]-1),
                 (at[0]+1, at[1]  ), (at[0]-1, at[1]  )]:
      if (y < 0 or y >= len(maze) or x < 0 or x >= len(maze[y])
          or maze[y][x] in walls or (x, y) in came_from):
        continue
      came_from[(x,y)] = at
      if (x, y) == finish:
        return came_from
      visit.append((x, y))
  return None

def print_solution(solution, sta, fin):
  if not solution:
    print('no solution :(')
  else:
    maze[sta[1]][sta[0]], maze[fin[1]][fin[0]] = '@', 'X'
    fin = solution[fin]
    while fin != sta:
      maze[fin[1]][fin[0]] = '.' if maze[fin[1]][fin[0]] != '.' else ' '
      fin = solution[fin]
    for row in maze:
      print(''.join(row))

if __name__ == '__main__':
  first = last = start = finish = None
  for y, s in enumerate(sys.stdin):
    row = []
    for x, c in enumerate(s.rstrip()):
      row.append(c)
      if c == '@':
        start = (x, y)
      elif c == 'X':
        finish = (x, y)
      elif c not in walls:
        if not first:
          first = (x, y)
        last = (x, y)
    maze.append(row)
  start, finish = start if start else first, finish if finish else last
  solution = solve(start, finish)
  print_solution(solution, start, finish)
