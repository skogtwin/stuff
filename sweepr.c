#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static char *progname = "sweepr";

void usage() {
  fprintf(stderr, "%s - simple terminal-based minesweeper game\n"
                  "usage: %s [OPTIONS]\n"
                  "options are:\n"
                  "  -b  number of bombs on the board\n"
                  "  -w  width of the board\n"
                  "  -h  hight of the board\n",
          progname, progname);
  exit(1);
}

int error(int fatal, char *err, ...) {
  va_list args;
  va_start(args, err);
  if (progname)
    fprintf(stderr, "%s: ", progname);
  vfprintf(stderr, err, args);
  va_end(args);
  if (err[strlen(err) - 1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  if (fatal)
    exit(1);
  return fatal;
}

void *ealloc(size_t nbytes) {
  void *p = calloc(nbytes, 1);
  if (p == NULL)
    error(1, "Cannot allocate %zd bytes:", nbytes);
  return p;
}


typedef struct {
  int nr, nc, nb, *f, *v;
} Field;

enum { BOMB = -1, FOW, CLEAR, FLAGGED };

void shuffle(int *arr, int len) {
  int i;
  for (i = 0; i < len; ++i) {
    int t, j = rand() % len;
    t = arr[i]; arr[i] = arr[j]; arr[j] = t;
  }
}

// rc2i transforms row-col pair to linear address while performing bound checks.
// r is row, c is col, nr is number of rows and nc is number of cols.
// returns non-negative index or -1 if coordinates are out of bounds.
int rc2i(int r, int nr, int c, int nc) {
  if (r < 0 || r >= nr || c < 0 || c >= nc)
    return -1;
  return r * nc + c;
}

Field newfield(int nrows, int ncols, int nbombs) {
  int r, c, len = nrows * ncols;
  Field f = {.nr = nrows, .nc = ncols, .nb = nbombs};

  /* allocate field and visibility mask to all zeroes, place bombs at first nbombs cells */
  f.v = ealloc(len * sizeof(*f.v));
  f.f = ealloc(len * sizeof(*f.f));
  for (r = 0; r < len; ++r)
    f.f[r] = r < nbombs ? BOMB : 0;

  /* shuffle the bombs */
  shuffle(f.f, len);

  /* populate empty cells with the number of bombs they are adjacent to */
  for (r = 0; r < f.nr; ++r)
    for (c = 0; c < f.nc; ++c) {
      int i = rc2i(r, f.nr, c, f.nc);
      if (f.f[i] == BOMB) {
        int dr, dc;
        for (dr = -1; dr <= 1; ++dr)
          for (dc = -1; dc <= 1; ++dc)
            if ((dc || dr) && (i = rc2i(r+dr, f.nr, c+dc, f.nc)) != -1 && f.f[i] != BOMB)
              f.f[i]++;
      }
    }

  return f;
}

void draw(const Field *f, int reveal) {
  int i, width = 1;
  char pad[0x20];

  for (i = f->nc; i > 0; i /= 10)
    ++width;

  for (i = 0; i < width && i < sizeof(pad)-1; ++i)
    pad[i] = ' ';
  pad[i] = '\0';


  fputs(pad, stdout);
  for (i = 0; i < f->nc; ++i)
    printf(&" %*d"[!i], width, i+1);
  fputc('\n', stdout);

  for (i = 0; i < f->nr * f->nc; ++i) {
    int c = '#';
    if (i%f->nc == 0)
      printf("%*d", width, i/f->nc + 1);
    if (f->v[i] == FLAGGED)
      c = 'F';
    if (f->v[i] == CLEAR || reveal) {
      switch (f->f[i]) {
      case BOMB:
        c = '*';
        break;
      case 0:
        c = '.';
        break;
      default:
        c = '0' + f->f[i];
        break;
      }
    }
    printf("%*c%s", width, c, i%f->nc == f->nc-1 ? "\n" : " ");
  }
}

void openzeros(Field *f, int r, int c) {
  int dr, dc, i;
  for (dr = -1; dr <= 1; ++dr)
    for (dc = -1; dc <= 1; ++dc)
      if ((dc || dr) && (i = rc2i(r+dr, f->nr, c+dc, f->nc)) != -1) {
        int v = f->v[i];
        f->v[i] = CLEAR;
        if (f->f[i] == 0 && v != CLEAR)
          openzeros(f, r+dr, c+dc);
      }
}

void play(Field f) {
  char op;
  const char *help = "flag cell: f <row> <col>\nreveal cell: r <row> <col>\n";
  const char *status = "";
  int i, r, c, rc, over = 0;
  for (;;) {
    draw(&f, over);
    fputs(status, stdout);
    if (over)
      break;
    fputs("> ", stdout);

    rc = fscanf(stdin, "%c %d %d", &op, &r, &c);
    if (rc == EOF) {
      printf("bye!\n");
      return;
    }
    while ((i = getc(stdin)) != EOF && i != '\n')
      ;
    if (rc != 3) {
      status = help;
      continue;
    }
    if ((i = rc2i(r-1, f.nr, c-1, f.nc)) == -1) {
      status = "out of bounds\n";
      continue;
    }

    switch (op) {
    case 'f': case 'F': /* flag cell */
      if (f.v[i] == CLEAR)
        status = "cannot flag open cells\n";
      else
        f.v[i] = (f.v[i] == FLAGGED) ? FOW : FLAGGED;
      break;
    case 'r': case 'R': /* reveal cell */
      if (f.v[i] == FLAGGED) {
        status = "flagged, won't open\n";
        break;
      }
      if (f.f[i] == BOMB) {
        status = "BOOM!\n";
        over = 1;
        break;
      }
      f.v[i] = CLEAR;
      if (f.f[i] == 0)
        openzeros(&f, r-1, c-1);
      break;
    default:
      status = help;
      break;
    }

    c = 0;
    for (i = 0; i < f.nc * f.nr; ++i)
      if (f.v[i] != CLEAR && (++c > f.nb))
        break;
    if (c == f.nb) {
      status = "All clear!\n";
      over = 1;
    }
  }
}

int main(int argc, char **argv) {
  int i, ncols = 9, nrows = 9, nbombs = 10;
  Field f;

  srand(time(NULL));

  for (i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-b") == 0) {
      if (argv[i+1] == NULL)
        usage();
      nbombs = atol(argv[++i]);
    } else if (strcmp(argv[i], "-w") == 0) {
      if (argv[i+1] == NULL)
        usage();
      ncols = atol(argv[++i]);
    } else if (strcmp(argv[i], "-h") == 0) {
      if (argv[i+1] == NULL)
        usage();
      nrows = atol(argv[++i]);
    } else {
      usage();
    }
  }

  f = newfield(nrows, ncols, nbombs);
  play(f);

  return 0;
}
