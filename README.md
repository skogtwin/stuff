# My stuff (and things)

This is a dumpster for small (mostly single-file) programs, that I consider
useful enough to keep, but not important enough to put into a separate repo. 

Many of these programs predate the repo by 10+ years and were lying somewhere
around on my HDDs (yup, HDDs) until I decided to save them.


## What I've got here

### embbin/
Tiny library for embedding small binary blobs into C programs.
Puts them on stack, so no more than SIZE_MAX bytes.

### lt100sloc/
__L__ess __T__han __100__ __S__ource __L__ines __O__f __C__ode.
As the name suggests, I put here various fun stuff that fits into 100 lines of
code or less.

### calc.c, calc2.c
Basic command-line calculator. The difference between it and most of the
other options is that it reads an expression from its arguments, not from file.
Sometimes it's just faster this way.
**calc2.c ** implements the same functionality but with Dijkstra's shunting yard
algorithim instead of recursive descent parser. Just to compare how much
faster/shorter/simpler it is.

### d6.c
Die roller that understands RPG notaion (e.g. 3d4, 6d6 etc.).

### fitmr.c
Programmable command-line timer.

I don't always work out, but when I do, I usually do exercises by the time,
not by repetitions. And this thing is real handy.

### gol.c
Text-mode Conway's Game of Life. I like to pipe my source code through
`tr -C '[:space:]' '#' | gol`.

### httpget.c
A very simple HTTP fetcher library, wrote this as a part of some disposable
web scraper, but decided to save it for future use. Works with HTTPS too.
Yes, I know that libcurl exists.

### lastn.c
Prints last N lines of a file of stdin. Basically `tail`, but simpler, more
limited and compiles on all platforms I can think of.

### lwrap.c
Wraps lines of stdin after particular width (default is 80, can be passed as an argument).

### permute.c
```
Usage: permute SEQUENCE
   or: permute ELEMENT0 .. ELEMENTn
Prints all the combinations of ELEMENTS0 .. ELEMENTSn
or characters in SEQUENCE (if you patient enough ofcourse)
```
I was playing one of the Shadowrun series CRPGs, and didn't find some of the
clues needed to crack a safe. So I decided to bruteforce it, since I had all the
numbers, just not their placements. But I have a short-term memory of a
goldfish, so I wrote this program to approach the task systematically :)
Have been using it couple of times since.

### prigraph.c
```
[me@computer]$ echo -e "0 what's up?\n1 not much\n2 i'm good\n3 kbay\n#\n0 1\n0 2\n1 3\n2 3\n" | prigraph
                   what's up?
                        |
    +-------------------+-------------------+
    |                                       |
not much                                i'm good
    |                                       |
    +-------------------+-------------------+
                        |
                      kbay
```
I use this one for copypasting small ASCII diagrams from my terminal
to the chats/messengers. It is crude, but it does the job.
I'm still considering making something serious out of it, but
I have been too lazy so far.

### prihist.c
Draws a simple horizontal histogram from a list of key-value pairs, one pair per
line. For piping output of `sort -c` to it.

### primeseq.c
This is a naïve implementation of Eratosthenes sieve. I don't remember
why I wrote this, but it was somehow related to an argument I had with a
collegue of mine.

### prno.c
```
Usage: prno IN_BASE OUT_BASE NUMBER...
Converts numbers between from base IN_BASE to base OUT_BASE.
Supports bases from 2 to 36 and R for roman numerals.
```
I don't know why, but I've been writing something like this on every computer
I happen to own/login to. There are, of course, more standard ways of doing it,
but the base case (without Roman numerals) is like 10 lines long, and it was
always easier for me to write this.

### sloc.c
```
usage: sloc [options] [file...]
output significant lines of code, i.e. skip empty lines and commentaries.
if no files given, reads from stdin.
options:
   -s LANGUAGE  syntax, LANGUAGE is one of c, python, lua
   -h           help, print this message and exit

```
Does what it says on the tin. Very rough, not a proper parser, but works for me.

### sponge.c
```
Usage: sponge <file>
Reads stdin into memory before writing it to <file>.
```
A simplified version of `sponge` tool from **moreutils** package.
Reads all *stdin* into memory then writes it to a *file*. Unlinke shell redirect
does not touch the file until after the input has been read.
Useful for modifying file in place (`grep <something> file | sed <something> | sponge file`).

### sudoku.c
```
Usage: sudoku [options] [file...]
Solves a sudoku puzzle. Fills in the board with a series of numbers from the
input, left-to-right, top-to-bottom.
Options are:
  -n <num>  stop after finding <num> solutions; default is 100, -1 disables
  -q        do not print solved boards, only the number of solutions found
  -h        print this message and exit
```
A simple sudoku solver; I mostly use it for checking if a puzzle has more than
one solution, which many other solvers do not do.

### sweepr.c
A simple terminal-based minesweeper game.
`r <row> <column>` to reveal, `f <row> <column>` to flag.
```
$ sweepr 
   1  2  3  4  5  6  7  8  9
 1 #  #  #  #  #  #  #  #  #
 2 #  #  #  #  #  #  #  #  #
 3 #  #  #  #  #  #  #  #  #
 4 #  #  #  #  #  #  #  #  #
 5 #  #  #  #  #  #  #  #  #
 6 #  #  #  #  #  #  #  #  #
 7 #  #  #  #  #  #  #  #  #
 8 #  #  #  #  #  #  #  #  #
 9 #  #  #  #  #  #  #  #  #
> r 1 1
   1  2  3  4  5  6  7  8  9
 1 .  .  .  1  #  #  #  #  #
 2 .  .  .  2  #  #  #  #  #
 3 .  .  .  1  #  #  #  #  #
 4 .  .  .  1  2  #  #  #  #
 5 .  .  .  .  1  #  3  2  1
 6 .  .  .  .  1  1  1  .  .
 7 1  1  1  .  .  .  .  .  .
 8 #  #  1  .  .  .  .  1  1
 9 #  #  1  .  .  .  .  1  #
> r 3 5
   1  2  3  4  5  6  7  8  9
 1 .  .  .  1  *  *  3  *  2
 2 .  .  .  2  3  3  3  *  2
 3 .  .  .  1  *  2  3  3  2
 4 .  .  .  1  2  3  *  *  1
 5 .  .  .  .  1  *  3  2  1
 6 .  .  .  .  1  1  1  .  .
 7 1  1  1  .  .  .  .  .  .
 8 1  *  1  .  .  .  .  1  1
 9 1  1  1  .  .  .  .  1  *
BOOM!
```


## Using my stuff

Feel free to. Most of the files mention MIT license, but I don't really care.

To compile just `make <filename_without_extension>` -- the default make rule
is sufficient most of the times. In the rare case when a library is required,
it is not hard to figure out. One example would be `gcc -o calc calc.c -lm`.


## Contributing

Okaaaaay, but why would you?
