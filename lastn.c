#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

const char *progname = "lastn";
static int lastn = 10;

void usage(int status) {
  FILE *out = (status == 0 ? stdout : stderr);
  fprintf(out, "%s prints last N lines of a file or stdin.\n", progname);
  fprintf(out, "usage: %s [-n N | -N] [file]\n", progname);
  exit(status);
}

void error(int status, const char *fmt, ...) {
  va_list args;

  va_start(args, fmt);
  fprintf(stderr, "%s: ", progname);
  vfprintf(stderr, fmt, args);
  if (fmt[strlen(fmt)-1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  va_end(args);

  if (status != 0)
    exit(status);
}

void *append(void *dst, int *sz, int *capa, const void *src, int len) {
  void *p;
  if (len == 0)
    return dst;
  if (*capa == 0) {
    *sz = *capa = len;
    return memmove(malloc(*capa), src, len);
  }
  if (*sz + len > *capa) {
    do {
      *capa *= 2;
    } while (*sz + len > *capa);
    if ((dst = realloc(dst, *capa)) == NULL)
      error(-1, "not enough memory to allocate %d bytes:", *capa);
  }
  memmove(dst + *sz, src, len);
  *sz += len;
  return dst;
}

void last(FILE *in) {
  int i, at, cur = 0;
  char s[0x200] = {'\0'};
  char **buffer;
  buffer = calloc(lastn, sizeof(*buffer));

  for (;;) {
    int len, sz = 0, capa = 0;
    char *p;
    do {
      p = fgets(s, sizeof(s), in);
      if (p == NULL)
        break;
      if (capa == 0)
        free(buffer[cur]);
      len = strlen(s);
      buffer[cur] = append(buffer[cur], &sz, &capa, s, len+1);
      --sz; /* undo trailing '\0' */
    } while(s[len-1] != '\n');
    if (p == NULL)
      break;
    cur = (cur+1)%lastn;
  }

  if (ferror(in))
    error(-1, "read error:");

  for (i = 0; i < lastn; ++i) {
    at = (cur+i)%lastn;
    if (buffer[at] != NULL) {
      fputs(buffer[at], stdout);
      free(buffer[at]);
    }
  }
  free(buffer);
}

int main(int argc, char **argv) {
  int i;
  FILE *fp = stdin;

  for (i = 1; i < argc; ++i) {
    if (argv[i][0] != '-')
      break;
    switch (argv[i][1]) {
    case 'h':
      usage(0);
      break;
    case 'n':
      lastn = atoi(argv[++i]);
      if (lastn == 0) {
        error(0, "illegal value of `-n' option: `%s'\n", argv[i]);
        usage(-1);
      }
      break;
    case '0': case '1': case '2': case '3': case '4': case '5':
    case '6': case '7': case '8': case '9':
      lastn = atoi(&argv[i][1]);
      if (lastn != 0)
        break;
    default:
      error(0, "invalid option `%s'\n", argv[i]);
      usage(-1);
      break;
    }
  }

  if (i < argc) {
    if ((fp = fopen(argv[i], "r")) == NULL)
      error(-1, "could not open `%s':", argv[i]);
  }

  last(fp);

  if (fp != stdin)
    fclose(fp);

  return 0;
}
