/* sponge.c - simplified version of sponge from moreutils package.
 * This file is distributed under MIT license
 * for details see http://opensource.org/licenses/MIT
 */ 

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char *progname = "sponge";

void usage() {
  printf("Usage: %s <file>\n"
         "Reads stdin into memory before writing it to <file>.\n",
         progname);
  exit(1);
}

static void error(int errcode, char *err, ...);
static void *append(void *dst, int *sz, int *capa, void *src, int n);

int main(int argc, char **argv) {
  char rbuf[0x1000], *buf = NULL;
  int sz = 0, capa = 0;
  FILE *fp;

  if (argc != 2) {
    usage();
  }

  while (!feof(stdin)) {
    size_t n = fread(rbuf, 1, sizeof(buf), stdin);
    if (n < sizeof(buf) && ferror(stdin)) {
      error(1, "read error:");
    }
    buf = append(buf, &sz, &capa, rbuf, n);
  }

  if ((fp = fopen(argv[1], "w")) == NULL) {
    error(1, "cannot open file %s:", argv[1]);    
  }

  if (fwrite(buf, 1, sz, fp) != sz) {
    error(1, "cannot write stdout:");
  }
  fclose(fp);

  free(buf);

  return 0;
}

void *append(void *dst, int *sz, int *capa, void *src, int n) {
  void *p;

  if (*sz + n > *capa) {
    *capa = *capa ? *capa*2 : n;
    p = realloc(dst, *capa);
    if (p == NULL) {
      error(1, "cannot allocate a buffer:");
    }
    dst = p;
  }

  memcpy(dst+*sz, src, n);
  *sz += n;
  return dst;
}

void error(int errcode, char *err, ...) {
  va_list args;
  va_start(args, err);
  if (progname)
    fprintf(stderr, "%s: ", progname);
  vfprintf(stderr, err, args);
  va_end(args);
  if (err[strlen(err) - 1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  if (errcode)
    exit(errcode);
}
