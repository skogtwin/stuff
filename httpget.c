/* httpget.c - primitive http(s) GET fetcher by skogtwin
 * distributed under MIT license. use at your own risk.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>


#ifdef HTTPGET_WITH_SSL

#include <openssl/ssl.h>

typedef struct {
  SSL_CTX *ctx;
  SSL *ssl;
} SSLstuff;

static SSLstuff *initssl(int fd);
static void cleanupssl(SSLstuff *);
static int sslwriteall(SSLstuff *ssl, char *s);
static char *sslreadall(SSLstuff *ssl);

#endif /* HTTPGET_WITH_SSL */

/* defaults */
#define QUERY "/"
#define PORT "80"
#define SPORT "443"

struct url {
  int secure;
  char *host;
  char *port;
  char *query;
};
static int parse(struct url *u, const char *url);
static void freesurl(struct url *u);
static int dial(const char *host, const char *port);
static char *saprintf(char *fmt, ...);
static int writeall(int fd, char *s);
static char *readall(int fd);

char *httpget(const char *url) {
  int fd = -1;
  char *req = NULL, *resp = NULL;
  struct url u;
  if (parse(&u, url) != 0)
    return NULL;
  if ((fd = dial(u.host, u.port)) < 0)
    goto OUT;
  req = saprintf("GET %s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n",
                 u.query, u.host);
  if (req == NULL)
    goto OUT;

#ifdef HTTPGET_WITH_SSL
{
  SSLstuff *ssl;
  if (u.secure) {
    if ((ssl = initssl(fd)) == NULL)
      goto OUT;
    if (sslwriteall(ssl, req) == 0)
      resp = sslreadall(ssl);
    cleanupssl(ssl);
    goto OUT;
  }
}
#endif

  if (writeall(fd, req) == 0)
    resp = readall(fd);

OUT:
  free(req);
  if (fd >= 0)
    close(fd);
  freesurl(&u);
  return resp;
}

char *readall(int fd) {
  char *ret, buf[0x200];
  int size = 0, capa = 0x200, nbytes;
  if ((ret = calloc(1, capa)) == NULL)
    return NULL;
  while ((nbytes = recv(fd, buf, sizeof(buf), 0)) > 0) {
    while (size + nbytes >= capa) {
      char *p;
      capa *= 2;
      p = realloc(ret, capa);
      if (p == NULL) {
        free(ret);
        return NULL;
      }
      ret = p;
    }
    memcpy(ret + size, buf, nbytes);
    size += nbytes;
    ret[size] = '\0';
  }
  if (nbytes == -1) {
    free(ret);
    return NULL;
  }
  return ret;
}

int writeall(int fd, char *s) {
  int nbytes, nwritten = 0, nleft = strlen(s);
  while (nleft > 0 && (nbytes = send(fd, s + nwritten, nleft, 0)) >= 0) {
    nwritten += nbytes;
    nleft -= nbytes;
  }
  return nleft;
}

char *saprintf(char *fmt, ...) {
   int n;
   char *ret;
   va_list ap;
   va_start(ap, fmt);
   n = vsnprintf(NULL, 0, fmt, ap);
   va_end(ap);
   ret = calloc(1, n + 1);
   if (ret != NULL) {
     va_start(ap, fmt);
     vsprintf(ret, fmt, ap);
     va_end(ap);
   }
   return ret;
}

char *alloccpy(char **dst, const char *src, int nbytes) {
  *dst = calloc(1, nbytes + 1);
  if (*dst != NULL)
    memcpy(*dst, src, nbytes);
  return *dst;
}

int expect(const char *tpl, const char **s) {
  const char *p = *s;
  while (*tpl != '\0' && *p == *tpl) {
    ++p; ++tpl;
  }
  if (*tpl == '\0') {
    if (p > *s)
      *s += p - *s;
    return 1;
  }
  return 0;
}

void freesurl(struct url *u) {
  free(u->host);
  free(u->port);
  free(u->query);
}

int parse(struct url *u, const char *url) {
  const char *p = url, *p1, *p2, *hostend, *urlend;
  if (url == NULL)
    return -1;
  memset(u, '\0', sizeof(*u));
  urlend = strchr(url, '\0');
  if (!expect("http", &p))
    return -1;
  if (expect("s", &p))
    u->secure = 1;
  if (!expect("://", &p))
    return -1;
  p1 = strchr(p, ':');
  p2 = strchr(p, '/');
  if (p1 != NULL && p2 != NULL && p1 < p2) {
    hostend = p1;
    if (alloccpy(&u->port, p1 + 1, p2 - p1 - 1) == NULL)
      goto NOMEM;
  } else if (p2 != NULL)
    hostend = p2;
  else
    hostend = urlend;
  if (hostend != urlend)
    if (alloccpy(&u->query,  p2, urlend - p2 + 1) == NULL)
      goto NOMEM;
  if (alloccpy(&u->host, p, hostend - p) == NULL)
    goto NOMEM;
  if (u->query == NULL)
    if (alloccpy(&u->query, QUERY, sizeof(QUERY) - 1) == NULL)
      goto NOMEM;
  if (u->port == NULL) {
    if (u->secure)
      p = alloccpy(&u->port, SPORT, sizeof(SPORT) - 1);
    else
      p = alloccpy(&u->port, PORT, sizeof(PORT) - 1);
    if (p == NULL)
      goto NOMEM;
  }
  return 0;
NOMEM:
  freesurl(u);
  return ENOMEM;
}

int dial(const char *host, const char *port) {
  int res, fd;
  struct addrinfo hints, *info, *p;
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;
  res = getaddrinfo(host, port, &hints, &info);
  if (res != 0)
    return -1;
  for (p = info; p != NULL; p = p->ai_next) {
    fd = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
    if (fd != -1 && connect(fd, p->ai_addr, p->ai_addrlen) != -1)
      break;
    else if (fd != -1)
      close(fd);
  }
  if (p == NULL)
    return -1;
  freeaddrinfo(info);
  return fd;
}

#ifdef HTTPGET_WITH_SSL

SSLstuff *initssl(int fd) {
  SSLstuff *ret;
  SSL_CTX *ctx;
  SSL *ssl;
  OpenSSL_add_ssl_algorithms();
  ctx = SSL_CTX_new(SSLv23_client_method());
  if (ctx == NULL)
    return NULL;
  ssl = SSL_new(ctx);
  if (ssl == NULL) {
    SSL_CTX_free(ctx);
    return NULL;
  }
  SSL_set_fd(ssl, fd);
  if (SSL_connect(ssl) == -1)
    goto ERROR;
  ret = calloc(sizeof(*ret), 1);
  if (ret == NULL)
    goto ERROR;
  ret->ssl = ssl;
  ret->ctx = ctx;
  return ret;
ERROR:
  SSL_CTX_free(ctx);
  SSL_free(ssl);
  return NULL;
}

static int sslwriteall(SSLstuff *ssl, char *s) {
  int n = strlen(s);
  return SSL_write(ssl->ssl, s, n) != n;
}

static char *sslreadall(SSLstuff *ssl) {
  char *ret, buf[0x200];
  int size = 0, capa = 0x200, nbytes;
  if ((ret = calloc(1, capa)) == NULL)
    return NULL;
  while ((nbytes = SSL_read(ssl->ssl, buf, sizeof(buf))) > 0) {
    while (size + nbytes >= capa) {
      char *p;
      capa *= 2;
      p = realloc(ret, capa);
      if (p == NULL) {
        free(ret);
        return NULL;
      }
      ret = p;
    }
    memcpy(ret + size, buf, nbytes);
    size += nbytes;
    ret[size] = '\0';
  }
  if (nbytes == -1) {
    free(ret);
    return NULL;
  }
  return ret;
}

void cleanupssl(SSLstuff *s) {
  if (s == NULL)
    return;
  if (s->ctx)
    SSL_CTX_free(s->ctx);
  if (s->ssl)
    SSL_free(s->ssl);
  free(s);
}

#endif

#ifdef HTTPGET_WITH_MAIN
int main(int argc, char **argv) {
  char *resp;
  resp = httpget(argv[1]);
  if (resp)
    printf("%s\n", resp);
  free(resp);
  return 0;
}
#endif
