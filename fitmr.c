/* fitmr.c - programmable command-line timer by skogtwin.
 * DSL is described as BNF at the end of the file.
 * This file is distributed under MIT license
 * for details see http://opensource.org/licenses/MIT
 */ 

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

const char *progname = "fitmr";

void usage() {
  fprintf(stderr, "Usage: %s [ALARM_PROGRAM]\n", progname);
  exit(1);
}

const char *alarm    = "echo BANG"; /* default way to signal about the event */

static FILE *tokstream; /* file stream to read program from */
static char tokbuf[15]; /* token buffer, needed for undoing more than 1 char */
static char bufpos = 0; /* position inside the buffer */
static int lineno = 1;  /* input script line number */

enum { EVENT = 1, SETLIST };

typedef struct _Set Set;
struct _Set {
  int repeat, pause;
  int type;
  union {
    Set *setlist;
    int duration;
  } v;
  Set *parent;
  Set *next;
};

static Set *programp;  /* pointer to first set of the program */

/* time_ms() returns some consistent timestamp in milliseconds. ****************
 * I know no real cross-platform way to implement it, so here be ifdefs.
 */
#if defined(_POSIX_C_SOURCE) && _POSIX_C_SOURCE >= 199309L

uint64_t time_ms() {
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  return (uint64_t) now.tv_sec * 1000 + now.tv_nsec / 1000000;
}

#elif defined(__MACH__)

#include <mach/clock.h>
#include <mach/mach.h>

uint64_t time_ms() {
  clock_serv_t cclock;
  mach_timespec_t now;

  host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
  clock_get_time(cclock, &now);
  mach_port_deallocate(mach_task_self(), cclock);

  return (uint64_t) now.tv_sec * 1000 + now.tv_nsec / 1000000;
}

#elif defined(_WIN32) || defined(WIN32)

#include <windows.h>

uint64_t time_ms() {
  LARGE_INTEGER freq, time;
  QueryPerformanceFrequency(&freq);
  QueryPerformanceCounter(&time);
  return (uint64_t) (time.QuadPart * 1000 / freq.QuadPart);
}

#else

uint64_t time_ms() {
  return (uint64_t) clock() * 1000 / CLOCKS_PER_SEC;
}

#endif
/*****************************************************************************/


int error(int fatal, char *err, ...) {
  va_list args;
  va_start(args, err);
  if (progname)
    fprintf(stderr, "%s: ", progname);
  vfprintf(stderr, err, args);
  va_end(args);
  if (err[strlen(err) - 1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  if (fatal)
    exit(fatal);
  return fatal;
}

void *ealloc(size_t nbytes) {
  void *p = calloc(nbytes, 1);
  if (p == NULL)
    error(1, "Cannot allocate %zd bytes:", nbytes);
  return p;
}

void fill() {
  int nb = fread(tokbuf, sizeof *tokbuf, sizeof tokbuf - 1, tokstream);
  if (ferror(tokstream))
    error(1, "Cannot read token stream:");
  tokbuf[nb] = '\0';
  bufpos = 0;
}

void refill() {
  int nb, half, pos;
  half = sizeof tokbuf / 2;
  memmove(tokbuf, tokbuf + half, sizeof tokbuf - half);
  pos = half;
  nb = fread(tokbuf + pos, sizeof *tokbuf, sizeof tokbuf - pos - 1, tokstream);
  if (ferror(tokstream))
    error(1, "Cannot read token stream:");
  if (nb == 0)
    tokbuf[bufpos] = '\0';
  else {
    bufpos = pos;
    tokbuf[bufpos + nb] = '\0';
  }
}

int nexttok() {
  if (tokbuf[bufpos] == '\0')
    refill();
  while (tokbuf[bufpos] != '\0' && isspace(tokbuf[bufpos])) {
    if (tokbuf[bufpos] == '\n')
      lineno++;
    bufpos++;
  }
  return tokbuf[bufpos++];
}

void undoc(int c) {
  if (bufpos == 0)
    error(2, "Undoing char on empty buffer\n");
  bufpos--;
}

void undos(char *s) {
  int len = strlen(s);
  if (len > bufpos)
    error(2, "Undoing overly long string\n");
  bufpos -= len;
}

int accept(int c) {
  int next_token = nexttok();
  if (next_token == c)
    return 1;
  undoc(next_token);
  return 0;
}

void expect(int c) {
  int next_token = nexttok();
  if (next_token != c)
    error(100, "Expected %c but got %c @%d\n", c, next_token, lineno);
}

int integer(char *s, int sz) {
  int i, c;
  for (i = 0; i < sz && (c = nexttok()) != '\0' && isdigit(c); ++i) {
    s[i] = c;
  }
  s[i] = '\0';
  if (c != '\0')
    undoc(c);
  return !!i;
}

int timespec(int spec) {
  char buf[0x200];
  if (integer(buf, sizeof buf) && accept(spec))
    return atoi(buf);
  undos(buf);
  return 0;
}

int duration() {
  int h, m, s;
  h = timespec('h');
  m = timespec('m');
  s = timespec('s');
  return h * 60 * 60 + m * 60 + s;
}

int repeat() {
  char buf[0x200] = {'\0'};
  if (accept('x') && integer(buf, sizeof buf))
    return atoi(buf);
  undos(buf);
  return 0;
}

int pause() {
  if (accept(':'))
    return duration();
  return 0;
}

Set *set_list(Set *p) {
  Set *head;
  extern Set *set(Set *);
  do {
    if (p == NULL)
      head = p = set(p);
    else
      p = set(p);
  } while (accept(','));
  return head;
}

Set *set(Set *setp) {
  Set *p = ealloc(sizeof *p);
  if (setp == NULL)
    setp = p;
  else {
    setp->next = p;
    setp = setp->next;
  }
  if (accept('(')) {
    setp->type = SETLIST;
    setp->v.setlist = set_list(setp->v.setlist);
    expect(')');
  } else {
    setp->type = EVENT;
    setp->v.duration = duration();
  }
  setp->repeat = repeat();
  if (setp->repeat == 0)
    setp->repeat = 1;
  setp->pause = pause();
  return setp;
}

void parse(FILE *fin) {
  tokstream = fin;
  fill();
  programp = set_list(programp);
}

void print_cur_time() {
  int nb;
  char buf[0x100];
  time_t t;
  struct tm *tmp;

  t = time(NULL);
  tmp = localtime(&t);
  if (tmp == NULL) {
    error(0, "localtime() failed:");
    return;
  }
  nb = strftime(buf, sizeof buf, "%F %T", tmp);
  buf[nb] = '\0';
  fputs(buf, stdout);
}

void signal_event() {
  int nb;
  char buf[0x200];
  FILE *fp = popen(alarm, "r");
  if (fp == NULL)
    error(1, "Cannot execute `%s':", alarm);
  while ((nb = fread(buf, sizeof *buf, sizeof buf, fp)) > 0)
    fwrite(buf, sizeof *buf, nb, stdout);
  pclose(fp);
}

void run_timer(int nsec) {
  int i = nsec, ndigits = 1;
  uint64_t now, finish;
  char fmt[10], s[80];

  while((i /= 10))
    ++ndigits;
  ndigits += 2; /* for one digit after the decimal point resolution */
  sprintf(fmt, "%%0%d.1f", ndigits);
  printf(fmt, duration);

  for (i = 0; i < sizeof s - 1 && i < ndigits; ++i)
    s[i] = '\b';
  s[i] = '\0';

  finish = time_ms() + nsec * 1000;
  while ((now = time_ms()) < finish) {
    fputs(s, stdout);
    printf(fmt, (double) (finish - now) / 1000);
  }
  fputs(s, stdout);
  print_cur_time();
  fputc('\n', stdout);
  signal_event();
}

void process_setl(Set *setp) {
  int i = 0;
  Set *p;
  for (p = setp; p != NULL; p = p->next) {
    switch (p->type) {
    case EVENT:
      for (i = 0; i < p->repeat; ++i) {
        run_timer(p->v.duration);
        if (p->pause)
          run_timer(p->pause);
      }
      break;
    case SETLIST:
      for (i = 0; i < p->repeat; ++i) {
        process_setl(p->v.setlist);
        if (p->pause && i != p->repeat - 1)
          run_timer(p->pause);
      }
      break;
    default:
      error(666, "Uknown event type: %d\n", p->type);
    }
  }
}

void execute() {
  process_setl(programp);
}

int main(int argc, char **argv) {
  if (argc > 2 || (argv[1] && strcmp(argv[1], "-h") == 0))
    usage();
  if (argc == 2)
    alarm = argv[1];
  parse(stdin);
  execute();
  return 0;
}

/*
fitmr BNF:

set-list ::= <set> "," <set-list> | <set>
     set ::= "(" <set-list> ")" <repeat> <pause> | <duration> <repeat> <pause>
duration ::= <h> <m> <s>
       h ::= <integer> "h" | ""
       m ::= <integer> "m" | ""
       s ::= <integer> "s" | ""
  repeat ::= "x" <integer> | ""
   pause ::= ":" <duration> | ""

EXAMPLE
One's morning workout is:
- 10s to get ready
- 5 sets of:
    - 5 x 30s of pushups, 15s of rest inbetween
    - 30s of rest
    - 5 x 30s of mountains, 5s of rest inbetween
    - 30s of rest
    - 5 x 30s of elbow crunches, 15s rest inbetween
  60s rest inbetween sets
- 3 x 30s of planking, 15s rest inbeween

fitmr script:
echo '10s, (30s x5:15s, 30s, 30s x5:5s, 30s, 30s x5:15s)x5:1m, 30s x3:15s' |
fitmr 'aplay ~/bebebeep.wav'
*/
