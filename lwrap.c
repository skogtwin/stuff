#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  int c, width = 80, curw = 0;
  if (argc == 2)
    width = atol(argv[1]);
  while ((c = fgetc(stdin)) != EOF) {
    putchar(c);
    if (++curw == width || c == '\n') {
      curw = 0;
      if (c != '\n')
        putchar('\n');
    }
  }
  return 0;
}
