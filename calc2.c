/* calc.c - simple command line calculator by skog.
 * Dijkstra's shunting yard algorithm.
 *
 * This file is distributed under MIT license
 * http://opensource.org/licenses/MIT
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


static const char *progname = "calc2";
static void usage() {
  fprintf(stderr, "usage: %s <expression>\n", progname);
  exit(1);
}

#define NELEMS(x) (sizeof(x)/sizeof(x[0]))


typedef struct {
  int prec, assoc; 
  void (*fn)(void);
} Op;

static struct {
  int sz;
  double vals[0x200];
} vals = {0};

static struct {
  int sz;
  Op ops[0x200];
} ops = {0};

void pushval(double val) {
  if (vals.sz > NELEMS(vals.vals)) {
    fprintf(stderr, "value stack overflow\n");
    exit(-1);
  }
  vals.vals[vals.sz++] = val;
}

double popval() {
  if (vals.sz <= 0) {
    fprintf(stderr, "value stack underflow\n");
    exit(-1);
  }
  return vals.vals[--vals.sz];
}

void pushop(Op op) {
  if (ops.sz >= NELEMS(ops.ops)) {
    fprintf(stderr, "operation stack overflow\n");
    exit(-1);
  }
  ops.ops[ops.sz++] = op;
}

Op popop() {
  if (ops.sz <= 0) {
    fprintf(stderr, "value stack underflow\n");
    exit(-1);
  }
  return ops.ops[--ops.sz];
}

/* built-in operations */
void op_add() {
  double r = popval(), l = popval();
  pushval(l + r);
}
void op_sub() {
  double r = popval(), l = popval();
  pushval(l - r);
}
void op_mul() {
  double r = popval(), l = popval();
  pushval(l * r);
}
void op_div() {
  double r = popval(), l = popval();
  pushval(l / r);
}
void op_pow() {
  double r = popval(), l = popval();
  pushval(pow(l, r));
}
void op_acos() {
  double r = popval();
  pushval(acos(r));
}
void op_asin() {
  double r = popval();
  pushval(asin(r));
}
void op_atan() {
  double r = popval();
  pushval(atan(r));
}
void op_atan2() {
  double r = popval(), l = popval();
  pushval(atan2(l, r));
}
void op_cos() {
  double r = popval();
  pushval(cos(r));
}
void op_cosh() {
  double r = popval();
  pushval(cosh(r));
}
void op_sin() {
  double r = popval();
  pushval(sin(r));
}
void op_sinh() {
  double r = popval();
  pushval(sinh(r));
}
void op_tan() {
  double r = popval();
  pushval(tan(r));
}
void op_tanh() {
  double r = popval();
  pushval(tanh(r));
}
void op_exp() {
  double r = popval();
  pushval(exp(r));
}
void op_log() {
  double r = popval();
  pushval(log(r));
}
void op_log10() {
  double r = popval();
  pushval(log10(r));
}
void op_log2() {
  double r = popval();
  pushval(log2(r));
}
void op_sqrt() {
  double r = popval();
  pushval(sqrt(r));
}
void op_abs() {
  double r = popval();
  pushval(abs(r));
}
void op_ceil() {
  double r = popval();
  pushval(ceil(r));
}
void op_floor() {
  double r = popval();
  pushval(floor(r));
}
void op_round() {
  double r = popval();
  pushval(round(r));
}
/* built-in operations end */


int parse_val(double *val, const char *s, int *at) {
  const char *start = &s[*at];
  char *end = (char *) start;
  double v = strtod(start, &end);
  if (end != start) {
    *val = v;
    *at = (int)((char*)end - (char*)s);
    return 1;
  }
  return 0;
}

int parse_op(Op *op, const char *s, int *at) {
  int i = *at;

  switch (s[i]) {
  case '+':
    op->prec = 2;
    op->assoc = -1;
    op->fn = op_add;
    *at = ++i;
    return 1;
  case '-':
    op->prec = 2;
    op->assoc = -1;
    op->fn = op_sub;
    *at = ++i;
    return 1;
  case '*':
    op->prec = 4;
    op->assoc = -1;
    op->fn = op_mul;
    *at = ++i;
    return 1;
  case '/':
    op->prec = 4;
    op->assoc = -1;
    op->fn = op_div;
    *at = ++i;
    return 1;
  case '^':
    op->prec = 6;
    op->assoc = 1;
    op->fn = op_pow;
    *at = ++i;
    return 1;
  }

  /* identifier: [_a-zA-Z][_a-zA-Z0-9]* */
  if (s[i] == '_' || isalpha(s[i])) {
    do {
      ++i;
    } while (s[i] == '_' || isalnum(s[i]));
  }

#define ADDFN(fname) if (!strncmp(#fname, &s[*at], i - *at)) {\
  op->prec = 8; op->assoc = -1; op->fn = op_##fname; *at = i; return 1;}
  ADDFN(acos);
  ADDFN(asin);
  ADDFN(atan);
  ADDFN(atan2);
  ADDFN(cos);
  ADDFN(cosh);
  ADDFN(sin);
  ADDFN(sinh);
  ADDFN(tan);
  ADDFN(tanh);
  ADDFN(exp);
  ADDFN(log);
  ADDFN(log10);
  ADDFN(log2);
  ADDFN(pow);
  ADDFN(sqrt);
  ADDFN(ceil);
  ADDFN(abs);
  ADDFN(floor);
  ADDFN(round);
#undef ADDFN
  return 0;
}

void eval_till(int prec, int assoc) {
  while (ops.sz > 0) {
    Op op = ops.ops[ops.sz-1];
    if (prec > op.prec || prec == op.prec && assoc == 1) {
      break;
    }
    op.fn();
    popop();
  }
}

void parse_and_eval(const char *s) {
  int i = 0;
  while (s[i] != '\0') {
    double val;
    Op op;
    /* skip separators */
    while (s[i] != '\0' && (isspace(s[i]) || s[i] == ','))
      ++i;
    /* parse */
    if (s[i] == ')') {
      eval_till(-1, 1);
      popop();
      ++i;
    } else if (s[i] == '(') {
      op.prec = -1;
      op.assoc = 1;
      op.fn = NULL;
      pushop(op);
      ++i;
    } else if (parse_val(&val, s, &i)) {
      pushval(val);
    } else if (parse_op(&op, s, &i)) {
      eval_till(op.prec, op.assoc);
      pushop(op);
    } else {
      fprintf(stderr, "could not parse expression starting from `%.10s'\n", &s[i]);
      exit(-1);
    }
  }
}

int main(int argc, char **argv) {
  if (argc == 1)
    usage();

  for (int i = 1; i < argc; ++i)
    parse_and_eval(argv[i]);
  eval_till(-1, 1);

  if (ops.sz != 0) {
    fprintf(stderr, "mismatched parenthesis\n");
    exit(-1);
  }
  printf("%g\n", vals.vals[0]);

  return 0;
}
