/* calc.c - simple command line calculator by skog.
 * recursive descent parser with immediate evaluation.
 * EBNF at the end of the file.
 *
 * This file is distributed under MIT license
 * http://opensource.org/licenses/MIT
 */
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

static const char *progname = "calc";
static void usage() {
  fprintf(stderr, "usage: %s <expression>\n", progname);
  exit(1);
}

/*
 * built-ins
 */
double fnacos(double *args) {
  return acos(args[0]);
}
double fnasin(double *args) {
  return asin(args[0]);
}
double fnatan(double *args) {
  return atan(args[0]);
}
double fnatan2(double *args) {
  return atan2(args[0], args[1]);
}
double fncos(double *args) {
  return cos(args[0]);
}
double fncosh(double *args) {
  return cosh(args[0]);
}
double fnsin(double *args) {
  return sin(args[0]);
}
double fnsinh(double *args) {
  return sinh(args[0]);
}
double fntan(double *args) {
  return tan(args[0]);
}
double fntanh(double *args) {
  return tanh(args[0]);
}
double fnexp(double *args) {
  return exp(args[0]);
}
double fnlog(double *args) {
  return log(args[0]);
}
double fnlog10(double *args) {
  return log10(args[0]);
}
double fnlog2(double *args) {
  return log2(args[0]);
}
double fnpow(double *args) {
  return pow(args[0], args[1]);
}
double fnsqrt(double *args) {
  return sqrt(args[0]);
}
double fnceil(double *args) {
  return ceil(args[0]);
}
double fnfabs(double *args) {
  return fabs(args[0]);
}
double fnfloor(double *args) {
  return floor(args[0]);
}
double fnround(double *args) {
  return round(args[0]);
}
double fnlist(double *args) {
  extern void listfns(void);
  listfns();
  return 0.0;
}

/*
 * misc functions
 */
int error(int fatal, char *err, ...) {
  va_list args;
  va_start(args, err);
  if (progname)
    fprintf(stderr, "%s: ", progname);
  vfprintf(stderr, err, args);
  va_end(args);
  if (err[strlen(err) - 1] == ':')
    fprintf(stderr, " %s", strerror(errno));
  fputc('\n', stderr);
  if (fatal)
    exit(1);
  return fatal;
}

void *ealloc(size_t nbytes) {
  void *p = calloc(nbytes, 1);
  if (p == NULL)
    error(1, "cannot allocate %zd bytes:", nbytes);
  return p;
}

void *erealloc(void *p, size_t nbytes) {
  void *p2 = realloc(p, nbytes);
  if (p2 == NULL)
    error(1, "cannot re-allocate %zd bytes:", nbytes);
  return p2;
}

char *astrcat(char *s0, int *sz, int *capa, char *s1) {
  int len = strlen(s1);
  if (*capa == 0) {
    s0 = ealloc(len);
    *capa = len;
    s0[0] = '\0';
    *sz = 1;
  } else {
    while (*capa - *sz - len <= 0) {
      *capa *= 2;
      s0 = erealloc(s0, *capa);
    }
  }
  *sz += len;
  return strcat(s0, s1);
}


/*
 * built-in functions lookup
 */
typedef struct {
  int nargs;
  char *name;
  double (*call)(double *args);
} Func;

static Func funcs[] = {
  { 1, "acos", fnacos },
  { 1, "asin", fnasin },
  { 1, "atan", fnatan },
  { 2, "atan2", fnatan2 },
  { 1, "cos", fncos },
  { 1, "cosh", fncosh },
  { 1, "sin", fnsin },
  { 1, "sinh", fnsinh },
  { 1, "tan", fntan },
  { 1, "tanh", fntanh },
  { 1, "exp", fnexp },
  { 1, "log", fnlog },
  { 1, "log10", fnlog10 },
  { 1, "log2", fnlog2 },
  { 2, "pow", fnpow },
  { 1, "sqrt", fnsqrt },
  { 1, "ceil", fnceil },
  { 1, "abs", fnfabs },
  { 1, "floor", fnfloor },
  { 1, "round", fnround },
  { 0, "list", fnlist }
};

Func *findfn(char *name) {
  int i;
  for (i = 0; i < sizeof(funcs) / sizeof(funcs[0]); ++i)
    if (strcmp(funcs[i].name, name) == 0)
      return &funcs[i];
  return NULL;
}

void listfns() {
  int i;
  for (i = 0; i < sizeof(funcs) / sizeof(funcs[0]); ++i)
    printf("%s\n", funcs[i].name);
}


/*
 * recursive descent parser
 */
void skipspaces(char **p) {
  while (isspace(**p))
    ++(*p);
}

void next(char **p) {
  ++(*p);
  skipspaces(p);
}

int accept(char **p, int c) {
  if (**p == c) {
    next(p);
    return 1;
  }
  return 0;
}

void expect(char **p, int c) {
  if (!accept(p, c))
    error(1, "expectation failed: %c != %c\n", c, **p);
}

double expr(char **p);

double number(char **p) {
  char *endp;
  double n = strtod(*p, &endp);
  if (endp == *p)
    error(1, "not a number: %.20s\n", *p);
  *p = endp;
  skipspaces(p);
  return n;
}

/* arg ::= <number> | '(' expr ')' */
double arg(char **p) {
  double n;
  if (accept(p, '(')) {
    n = expr(p);
    expect(p, ')');
    return n;
  }
  return number(p);
}

/* fname ::= 'a'-'Z'('a'-'Z''0'-'9')* */
char *fname(char *namebuf, int capa, char **p) {
  int i = 0;
  if (isalpha(**p)) {
    namebuf[i++] = **p;
    for ((*p)++; isalnum(**p) && i + 1 < capa; (*p)++)
      namebuf[i++] = **p;
    namebuf[i] = '\0';
    if (i + 1 == capa && isalnum(**p))
      error(1, "function name is too long: `%s...'\n", namebuf);
    skipspaces(p);
    return namebuf;
  }
  return NULL;
}

/* term2 ::= arg | <fname> '(' <args> ')' */
double term2(char **p) {
  char fnbuf[0x80] = {'\0'};
  if (fname(fnbuf, sizeof(fnbuf), p) != NULL) {
    int i;
    double *args, n;
    Func *f = findfn(fnbuf);
    if (f == NULL)
      error(1, "unknown function `%s'\n", fnbuf);
    expect(p, '(');
    args = ealloc(f->nargs * sizeof(*args));
    for (i = 0; i < f->nargs; ++i) {
      args[i] = expr(p);
      if (i + 1 < f->nargs)
        expect(p, ',');
    }
    expect(p, ')');
    n = f->call(args);
    free(args);
    return n;
  }
  return arg(p);
}

/* term1 ::= <term2> | <term2> (op2 <term2>)+ */
double term1(char **p) {
  double n = term2(p);
  int op = **p;
  while (op == '^') {
    next(p);
    double m = term2(p);
    n = pow(n, m);
    op = **p;
  }
  return n;
}

/* term0 ::= <term1> | <term1> (op1 <term1>)+ */
double term0(char **p) {
  double n = term1(p);
  int op = **p;
  while (op == '*' || op == '/' || op == '%') {
    next(p);
    double m = term1(p);
    switch (op) {
    case '*':
      n *= m;
      break;
    case '/':
      if (m == 0)
        error(1, "division by 0");
      n /= m;
      break;
    case '%':
      if (m == 0)
        error(1, "division by 0");
      n = fmod(n, m);
      break;
    }
    op = **p;
  }
  return n;
}

/* expr ::= <term0> | <term0> (op0 <term0>)+ */
double expr(char **p) {
  double n = term0(p);
  int op = **p;
  while (op == '+' || op == '-') {
    next(p);
    double m = term0(p);
    switch (op) {
    case '+':
      n += m;
      break;
    case '-':
      n -= m;
      break;
    }
    op = **p;
  }
  return n;
}


/*
 * connect the pieces
 */
int main(int argc, char **argv) {
  int i, sz = 0, capa = 0;
  double res;
  char *ex, *p;
  if (argc == 1)
    usage();
  for (i = 1; i < argc; ++i)
    ex = astrcat(ex, &sz, &capa, argv[i]);
  p = ex;
  skipspaces(&p);
  res = expr(&p);
  printf("%g\n", res);
  return 0;
}


/* EBNF ***
 *
 *  expr ::= <term0> | <term0> (op0 <term0>)+
 *  term0 ::= <term1> | <term1> (op1 <term1>)+
 *  term1 ::= <term2> | <term2> (op2 <term2>)+
 *  term2 ::= arg | <fname> '(' <args> ')'
 *  args ::= '' | <expr> (',' <expr>)*
 *  arg ::= <number> | '(' expr ')'
 *  op0 ::= '+' | '-'
 *  op1 ::= '*' | '/' | '%'
 *  op2 ::= '^'
 *  fname ::= 'a'-'Z'('a'-'Z''0'-'9')*
 *
 * EBNF ***/
